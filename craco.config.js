const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { 
              '@primary-color': '#A30101',
              '@header-color': '#E30016',
              '@sider-color': 'white',
              "@font-family": "'Poppins','Montserrat', sans-serif",
              // "@text-color": '#707070'
              
          
          },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};