import { Card, Row, Col, Image, Button, PageHeader, Divider, Input, Form, DatePicker, InputNumber, Typography, Space, Select, } from 'antd';
import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import IconSubmitCheck from '../../assets/icons/icon_submit_check.svg';
import ImgTrainingAvatar from '../../assets/images/img_training_avatar.png';
import { useState } from 'react';

//button icon handle

const { Option } = Select;
const { RangePicker } = DatePicker;
const { Text } = Typography;

const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select time!' }],
};

function TrainingReportComp() {
    const [size] = useState(12);

    const formItemLayout = {
        labelCol: {
            span: 24,
        },
        wrapperCol: {
            span: 24,
        }
    };


    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-training-picker'];
        const values = {
            ...fieldsValue,
            'range-training-picker': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

        };
        console.log('Received values of form: ', values);
    };

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Training Report"
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>

                <Form name="time_related_controls" onFinish={onFinish}{...formItemLayout}
                >
                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="What is learned from training"
                        name="whatislearnedfromtraining"
                    >
                        <Input.TextArea style={{ height: 100 }} />
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="Benefit for Personal Development"
                        name="benefitforpersonaldevelopment"
                    >
                        <Input.TextArea style={{ height: 100 }} />
                    </Form.Item>
                    <Row>
                        <Col span={11} md={24} lg={11}>
                            <Form.Item
                                name="trainingmaterialvalue"
                                label="Training material value"
                                hasFeedback

                            >

                                <Select placeholder="Please select value"  >
                                    <Option value="5">5 - Sangat Bagus</Option>
                                    <Option value="4">4 - Bagus</Option>
                                    <Option value="3">3 - Biasa</Option>
                                    <Option value="2">2 - Buruk</Option>
                                    <Option value="1">1 - Sangat Buruk</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={2} lg={2}></Col>

                        <Col span={11} md={24} lg={11}>
                            <Form.Item
                                name="trainercapability"
                                label="Trainer capability"
                                hasFeedback
                            >
                                <Select placeholder="Please select value" >
                                    <Option value="5">5 - Sangat Bagus</Option>
                                    <Option value="4">4 - Bagus</Option>
                                    <Option value="3">3 - Cukup</Option>
                                    <Option value="2">2 - Buruk</Option>
                                    <Option value="1">1 - Sangat Buruk</Option>
                                </Select>
                            </Form.Item>

                        </Col>
                    </Row>
                    <Row>
                        <Col span={11} md={24} lg={11}>
                            <Form.Item
                                name="facilitycondition"
                                label="Facility condition"
                                hasFeedback
                            >
                                <Select placeholder="Please select value">
                                    <Option value="5">5 - Sangat Bagus</Option>
                                    <Option value="4">4 - Bagus</Option>
                                    <Option value="3">3 - Moderate</Option>
                                    <Option value="2">2 - Buruk</Option>
                                    <Option value="1">1 - Sangat Buruk</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={2} lg={2}></Col>

                        <Col span={11} md={24} lg={11}>
                            <Form.Item
                                name="benefitforcompany"
                                label="Benefit for company"
                                hasFeedback
                            >
                                <Select placeholder="Please select value" >
                                    <Option value="5">5 - Sangat Bermanfaat</Option>
                                    <Option value="4">4 - Bermanfaat</Option>
                                    <Option value="3">3 - Cukup Bermanfaat</Option>
                                    <Option value="2">2 - Kurang Bermanfaat</Option>
                                    <Option value="1">1 - Tidak Bermanfaat</Option>
                                </Select>
                            </Form.Item>

                        </Col>
                    </Row>

                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="Feedback"
                        name="feedback"
                    >
                        <Input.TextArea style={{ height: 100 }} />
                    </Form.Item>


                    <div>
                        <Card bordered={false} className="listCardGrey">
                            <Form.Item
                                labelCol={{ span: 24 }}
                                label="Training Name"
                                name="trainingname"
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                labelCol={{ span: 24 }}
                                label="Training Description"
                                name="trainingdescription"
                            >
                                <Input.TextArea style={{ height: 100 }} />
                            </Form.Item>
                            <Form.Item labelCol={{ span: 24 }} name="range-training-picker" label="Training Schedule" {...rangeConfig}>
                                <RangePicker />
                            </Form.Item>

                            <Form.Item
                                labelCol={{ span: 24 }}
                                label="Place"
                                name="place"
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                labelCol={{ span: 24 }}
                                label="Estimated Cost"
                                name="estimatedcost"
                            >
                                <InputNumber />
                            </Form.Item>
                            <Form.Item
                                labelCol={{ span: 24 }}
                                label="Outcome"
                                name="outcome"
                            >
                                <Input.TextArea style={{ height: 100 }} />
                            </Form.Item>
                        </Card>
                    </div>
                    <div>
                        <Card bordered={false} className="listCardGreySmall">
                            <h3 style={{ color: "#4C62A2" }}>Approval Notes</h3>
                            <Row justify="start" align="middle" style={{ marginBottom: 15 }}>
                                <Space size={size}>
                                    <Col >
                                        <Row>
                                            <Image src={ImgTrainingAvatar} preview={false} height={60} />
                                        </Row>
                                    </Col>
                                    <Col >
                                        <Row justify="start" >
                                            <Col style={{ fontSize: 15, fontWeight: 500 }}>You</Col>
                                        </Row>
                                        <Row justify="start">
                                            <Col style={{ fontSize: 13.5, color: "#707070" }}>21 Juli 2021 17:05</Col>
                                        </Row>
                                    </Col>
                                </Space>
                            </Row>
                            <Form.Item
                                labelCol={{ span: 24 }}
                                label={false}
                                name="approvalnotes"
                            >
                                <Input />
                            </Form.Item>
                        </Card>
                        <Form.Item
                            labelCol={{ span: 24 }}
                        >
                            <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                <Row align="middle" justify="start">
                                    <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                    <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                                </Row>
                            </Button>
                        </Form.Item>
                    </div>
                </Form>
            </Card>
        </div >
    )
}

export default TrainingReportComp
