import { Card, Row, Col, Image, Button, Divider, PageHeader, Input, Form, Alert, Collapse, Select, Typography, Space, DatePicker, InputNumber } from 'antd';
import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import IconSubmitCheck from '../../assets/icons/icon_submit_check.svg';
import ImgTrainingAvatar from '../../assets/images/img_training_avatar.png';

import { useState } from 'react';

const { RangePicker } = DatePicker;

const rangeConfig = {
  rules: [{ type: 'array' as const, required: true, message: 'Please select time!' }],
};
const { Panel } = Collapse;

function callback(key: any) {
  console.log(key);
}
const { Option } = Select;
const { Text } = Typography;


function TrainingReviewComp() {
  const [size] = useState(12);

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    }
  };

  const onFinish = (fieldsValue: any) => {

    // Should format date value before submit.
    const rangeValue = fieldsValue['range-training-picker'];
    const values = {
      ...fieldsValue,
      'range-training-picker': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

    };
    console.log('Received values of form: ', values);
  };

  return (
    <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
      <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
            onBack={() => window.history.back()}
            title="Training Review"
          >
          </PageHeader>
          <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
        </div>

        <Form name="time_related_controls" onFinish={onFinish}{...formItemLayout}
        >
          <Row >
            <Col span={11} md={24} lg={11}>

              <Form.Item
                labelCol={{ span: 24 }}
                label="Employee Name"
                name="employeename"
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={2} lg={2}></Col>
            <Col span={11} md={24} lg={11}>

              <Form.Item
                labelCol={{ span: 24 }}
                label="Training Name"
                name="trainingname"
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>

          <div >
            {/* Penilaian Tahap 1 */}

            <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardPenilaian" >
              <Panel header="Penilaian Tahap 1" key="1" >
                <Alert style={{ paddingLeft: 23 }}
                  message="Petunjuk Pengerjaan :"
                  description={<div>
                    <Row >
                      1.&nbsp; Penilai mengamati perilaku Pegawai yang sudah mengikuti diklat
                    </Row>
                    <Row>
                      2. Berikan penilaian atas indikator perilaku yang menjadi point utama materi diklat
                    </Row>
                    <Row>
                      3. Penilaian 1-10 berdasarkan intensitas perilaku jarang sampai dengan sering dilakukan
                    </Row>

                  </div>}
                  type="warning"
                />
                <Card bordered={false} className="CardPerubahanPerilaku" style={{ marginTop: 20 }}>
                  <h3 style={{ color: "#5067AA", fontWeight: 600 }}>Perubahan Perilaku</h3>
                  <Divider style={{ border: '1.5px solid #5067AA', margin: '8px 0px 30px 0px', padding: 0, backgroundColor: "#5067AA" }} />

                  <Row justify="space-between" >
                    <Col lg={16}>
                      Pegawai berkomunikasi dengan rekan kerja dengan sopan
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap1komunikasidenganrekankerja"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10" >10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>

                  <Row justify="space-between" >
                    <Col lg={16}>
                      Pegawai berkomunikasi dengan atasan dengan sopan
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap1komunikasidenganatasan"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Pegawai menyesuaikan diri ketika berkomunikasi dengan orang lain
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap1menyesuaikankomunikasidenganoranglain"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                </Card>
              </Panel>
            </Collapse>
            {/* Penilaian Tahap 2 */}

            <Collapse defaultActiveKey={['2']} onChange={callback} bordered={false} className="cardPenilaian" style={{ marginTop: 40 }}>

              <Panel header="Penilaian Tahap 2" key="2">
                <Alert style={{ paddingLeft: 23 }}
                  message="Petunjuk Pengerjaan :"
                  description={<div>
                    <Row>
                      1.&nbsp; Penilai mengamati perilaku Pegawai yang sudah mengikuti diklat
                    </Row>
                    <Row>
                      2. Berikan penilaian atas indikator perilaku berdasarkan kategori mau & bisa

                    </Row>
                    <Row>
                      3. Penilaian menggunakan tanda silang pada kolom yang paling menggambarkan perilaku pegawai
                    </Row>

                  </div>}
                  type="warning"
                />
                <Card bordered={false} className="CardPerubahanPerilaku" style={{ marginTop: 20 }}>
                  <h3 style={{ color: "#5067AA", fontWeight: 600 }}>Perubahan Perilaku</h3>
                  <Divider style={{ border: '1.5px solid #5067AA', margin: '8px 0px 30px 0px', padding: 0, backgroundColor: "#5067AA" }} />
                  {/* <Space size={size} direction="vertical"> */}


                  <Row justify="space-between" style={{ marginBottom: 5 }}>
                    <Col lg={16}>
                      <Row>
                        1.&nbsp; Planning - perencanaan tugas
                      </Row>
                      <Row style={{ marginLeft: 15 }}>
                        Pegawai dapat melakukan perencanaan tugas sesuai dengan sistematika, waktu dan target yang sudah dilakukan
                      </Row>
                    </Col>
                    <Col lg={8}>
                      <Row justify="end" align="bottom">
                        <Form.Item
                          name="tahap2planning"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  {/* <Divider /> */}

                  <Row justify="space-between" style={{ marginBottom: 5 }}>
                    <Col lg={16}>
                      <Row>
                        2. Organizing - pengorganisasian tugas
                      </Row>
                      <Row style={{ marginLeft: 15 }}>
                        Pegawai dapat mengorganisir tugas sesuai dengan konsep 5W1H sehingga pegawai dapat mencapai target yang diwajibkan
                      </Row>
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap2organizing"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" style={{ marginBottom: 5 }}>
                    <Col lg={16}>
                      <Row>
                        3. Actuating - pengimplementasian / pelaksanaan tugas
                      </Row>
                      <Row style={{ marginLeft: 15 }}>
                        Pegawai melaksanakan dengan tepat, cepat, efektif dan efisien pekerjaan yang sudah di rencanakan                      </Row>

                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap2actuating"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" style={{ marginBottom: 5 }}>
                    <Col lg={16}>
                      <Row>
                        4. Controlling - pemantauan / evaluasi diri terhadap penyelesaian tugas
                      </Row>
                      <Row style={{ marginLeft: 15 }}>
                        Pegawai melakukan evaluasi atas apa yang sudah diselesaikan, sehingga pegawai dapat memperbaiki untuk pekerjaan selanjutnya </Row>

                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap2controlling"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  {/* </Space> */}

                </Card>
              </Panel>


            </Collapse>
            {/* Penilaian Tahap 3 */}

            <Collapse defaultActiveKey={['3']} onChange={callback} bordered={false} className="cardPenilaian" style={{ marginTop: 40 }}>

              <Panel header="Penilaian Tahap 3" key="3">
                <Alert style={{ paddingLeft: 23 }}
                  message="Petunjuk Pengerjaan :"
                  description={<div>
                    <Row>
                      1.&nbsp; Penilai mengamati perilaku pegawai secara umum
                    </Row>
                    <Row>
                      2. Berikan masukan untuk pengembangan diri sesuai dengan pilihan kompetensi pegawai

                    </Row>
                    <Row>
                      3. Penilaian menggunakan tanda silang pada kolom yang paling menggambarkan perilaku pegawai

                    </Row>
                    <Row>
                      4. Penilaian 1-10 berdasarkan intensitas perilaku tidak baik sampai dengan sangat baik
                    </Row>
                  </div>}
                  type="warning"
                />
                <Card bordered={false} className="CardPerubahanPerilaku" style={{ marginTop: 20 }}>
                  <h3 style={{ color: "#5067AA", fontWeight: 600 }}>Perubahan Perilaku</h3>
                  <Divider style={{ border: '1.5px solid #5067AA', margin: '8px 0px 30px 0px', padding: 0, backgroundColor: "#5067AA" }} />

                  <Row justify="space-between" >
                    <Col lg={16}>
                      Kerjasama
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3kerjasama"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  {/* <Divider /> */}

                  <Row justify="space-between" >
                    <Col lg={16}>
                      Perekat bangsa
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3perekatbangsa"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Komunikasi
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3komunikasi"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Pelayanan publik
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3pelayananpublik"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Integritas
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3integritas"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Orientasi pada hasil
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3orientasipadahasil"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Mengelola perubahan
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3mengelolaperubahan"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                  <Row justify="space-between" >
                    <Col lg={16}>
                      Pengambilan keputusan
                    </Col>
                    <Col lg={8}>
                      <Row justify="end">
                        <Form.Item
                          name="tahap3pengambilankeputusan"
                          label=""
                          hasFeedback
                        >
                          <Select placeholder="Select Score" style={{ marginRight: 25, }}>
                            <Option value="10">10</Option>
                            <Option value="9">9</Option>
                            <Option value="8">8</Option>
                            <Option value="7">7</Option>
                            <Option value="6">6</Option>
                            <Option value="5">5</Option>
                            <Option value="4">4</Option>
                            <Option value="3">3</Option>
                            <Option value="2">2</Option>
                            <Option value="1">1</Option>
                          </Select>
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                </Card>
              </Panel>
            </Collapse>
          </div>

          <div style={{ margin: "40px 0px 40px 0px" }}>
            <Card bordered={false} className="listCardGrey">
              <Row >
                <Col span={11} md={24} lg={11}>

                  <Form.Item
                    labelCol={{ span: 24 }}
                    label="Employee Name"
                    name="employeenameapproval"
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={2} lg={2}></Col>
                <Col span={11} md={24} lg={11}>

                  <Form.Item
                    labelCol={{ span: 24 }}
                    label="Training Name"
                    name="trainingnameapproval"
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item
                labelCol={{ span: 24 }}

                label="Training Description"
                name="trainingdescription"
              >
                <Input.TextArea style={{ height: 100 }} />
              </Form.Item>
              <Form.Item labelCol={{ span: 24 }} name="range-training-picker" label="Training Schedule" {...rangeConfig}>
                <RangePicker />
              </Form.Item>

              <Form.Item
                labelCol={{ span: 24 }}

                label="Place"
                name="place"
              >
                <Input />
              </Form.Item>
              <Form.Item
                labelCol={{ span: 24 }}

                label="Estimated Cost"
                name="estimatedcost"
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                labelCol={{ span: 24 }}

                label="Outcome"
                name="outcome"
              >
                <Input.TextArea style={{ height: 50 }} />
              </Form.Item>




            </Card>
          </div>

          <Card bordered={false} className="listCardGreySmall">
            <h3 style={{ color: "#4C62A2", marginBottom: 10 }}>Approval Notes</h3>
            <Row justify="start" align="middle" style={{ marginBottom: 15 }}>
              <Space size={size}>
                <Col >
                  <Row>
                    <Image src={ImgTrainingAvatar} preview={false} height={60} />
                  </Row>
                </Col>
                <Col >
                  <Row justify="start" >
                    <Col style={{ fontSize: 15, fontWeight: 500 }}>You</Col>
                  </Row>
                  <Row justify="start">
                    <Col style={{ fontSize: 13.5, color: "#707070" }}>21 Juli 2021 17:05</Col>
                  </Row>
                </Col>
              </Space>
            </Row>
            <Form.Item
              labelCol={{ span: 24 }}
              label={false}
              name="approvalnotes"
            >
              <Input />
            </Form.Item>
          </Card>

          <Card bordered={false} className="listCardGreySmall" style={{ marginTop: 40 }}>

            <h3 style={{ color: "#4C62A2", marginBottom: 10 }}>Training Report</h3>
            <Form.Item
              labelCol={{ span: 24 }}
              label="What is learned from training"
              name="whatislearnedfromtraining"
            >
              <Input.TextArea style={{ height: 50 }} />
            </Form.Item>
            <Form.Item
              labelCol={{ span: 24 }}
              label="Benefit for Personal Development"
              name="benefitforpersonaldevelopment"
            >
              <Input.TextArea style={{ height: 50 }} />
            </Form.Item>
            <Row>
              <Col span={11} md={24} lg={11}>
                <Form.Item
                  name="trainingmaterialvalue"
                  label="Training material value"
                  hasFeedback
                >

                  <Select placeholder="Please select value"  >
                    <Option value="5">5 - Sangat Bagus</Option>
                    <Option value="4">4 - Bagus</Option>
                    <Option value="3">3 - Biasa</Option>
                    <Option value="2">2 - Buruk</Option>
                    <Option value="1">1 - Sangat Buruk</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={2} lg={2}></Col>

              <Col span={11} md={24} lg={11}>
                <Form.Item
                  name="trainercapability"
                  label="Trainer capability"
                  hasFeedback
                >
                  <Select placeholder="Please select value" >
                    <Option value="5">5 - Sangat Bagus</Option>
                    <Option value="4">4 - Bagus</Option>
                    <Option value="3">3 - Cukup</Option>
                    <Option value="2">2 - Buruk</Option>
                    <Option value="1">1 - Sangat Buruk</Option>
                  </Select>
                </Form.Item>

              </Col>
            </Row>
            <Row>
              <Col span={11} md={24} lg={11}>
                <Form.Item
                  name="facilitycondition"
                  label="Facility condition"
                  hasFeedback
                >
                  <Select placeholder="Please select value">
                    <Option value="5">5 - Sangat Bagus</Option>
                    <Option value="4">4 - Bagus</Option>
                    <Option value="3">3 - Moderate</Option>
                    <Option value="2">2 - Buruk</Option>
                    <Option value="1">1 - Sangat Buruk</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={2} lg={2}></Col>

              <Col span={11} md={24} lg={11}>
                <Form.Item
                  name="benefitforcompany"
                  label="Benefit for company"
                  hasFeedback
                >
                  <Select placeholder="Please select value" >
                    <Option value="5">5 - Sangat Bermanfaat</Option>
                    <Option value="4">4 - Bermanfaat</Option>
                    <Option value="3">3 - Cukup Bermanfaat</Option>
                    <Option value="2">2 - Kurang Bermanfaat</Option>
                    <Option value="1">1 - Tidak Bermanfaat</Option>
                  </Select>
                </Form.Item>

              </Col>
            </Row>

            <Form.Item
              labelCol={{ span: 24 }}
              label="Feedback"
              name="feedback"
            >
              <Input.TextArea style={{ height: 50 }} />
            </Form.Item>
          </Card>


          <Form.Item
            labelCol={{ span: 24 }}
          >
            <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
              <Row align="middle" justify="start">
                <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
              </Row>
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div >
  )
}

export default TrainingReviewComp
