import { Card, Table, PageHeader, Button, Divider, Input, Tag, Menu, Dropdown, Typography, Form, DatePicker } from 'antd';
import { useState } from 'react';
import IconBackArrow from '../../../assets/icons/icon_back_arrow.svg';
import IconDownload from '../../../assets/icons/icon_download.svg';
import IconPrint from '../../../assets/icons/icon_print.svg';
import { DownOutlined } from '@ant-design/icons';

const { Column, } = Table;

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

//button icon handle
const onSearch = (value: any) => console.log(value);

const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}
const { RangePicker } = DatePicker;
const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select date!' }],
};

function TeamAppraisalComp() {
    // let { url } = useRouteMatch();

    const { Search } = Input;
    const { Text } = Typography;

    const [data] = useState([
        {
            key: '1',
            membername: 'Stephanus',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '2',
            membername: 'Rudy',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '3',
            membername: 'Hendra',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '4',
            membername: 'Sugimin',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '5',
            membername: 'Pareto',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '6',
            membername: 'Andy Leonard',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '7',
            membername: 'Deki',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['on-finalisasi'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        }, {
            key: '8',
            membername: 'Budi',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['on-finalisasi'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        }, {
            key: '9',
            membername: 'Samsul',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        }, {
            key: '10',
            membername: 'Sugiono',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '11',
            membername: 'Marijan',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
        {
            key: '12',
            membername: 'Suritem',
            lastupdate: '6 Jun 2019',
            currentstep: 'Mid-Year Review',
            status: ['revise'],
            remarks: 'Revise the mid year Review',
            detail: 'detail',
        },
    ]);

    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-period'];
        const values = {
            ...fieldsValue,
            'range-period': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

        };
        console.log('Received values of form: ', values);
    };

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
            <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Team Appraisal"
                        // subTitle="This is a subtitle"
                        extra={[
                            // <Button key="3">Operation</Button>,
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload"/></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint"/></Button>

                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
                <Search placeholder="Search Team Member" onSearch={onSearch} bordered={false} className="searchLearning" style={{ padding: 10, marginTop: 39, borderStyle: 'none' }} />
                <Form name="time_related_controls" onFinish={onFinish}
                >
                    <Form.Item labelCol={{ span: 24 }}
                        name="range-period" label="Period" {...rangeConfig}>
                        <RangePicker />
                    </Form.Item>
                    {/* <Form.Item
                        labelCol={{ span: 24 }}
                    >
                        <Row>
                            <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                <Row align="middle" justify="start">
                                    <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                    <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                                </Row>
                            </Button>
                        </Row>
                    </Form.Item> */}
                </Form>
                <Table dataSource={data}
                    onRow={(record, rowIndex) => {
                        return {
                            onClick: () => {
                                console.log("row clicked");
                            }, // click row

                        };
                    }}
                >

                    <Column
                        title="Member Name"
                        dataIndex="membername"
                        key="membername"
                        // responsive={['md']}
                        render={(membername) => (
                            <>
                                {
                                    membername

                                    // membername ? (
                                    //     <Link to={{
                                    //         pathmembername: `${url}/detailnama/`,
                                    //         state: { valListJobCompetency: { membername } }
                                    //     }} style={{ color: "black", fontSize: 13 }}>{membername}</Link>
                                    // ) : (
                                    //     <p>No data found</p>
                                    // )
                                }
                            </>
                        )}
                    />
                    <Column
                        title="Last Update"
                        dataIndex="lastupdate"
                        key="lastupdate"
                        // responsive={['md']}
                            
                        render={(lastupdate) => (
                            <>{lastupdate}</>
                        )}
                    />
                    <Column
                        title="Current Step"
                        dataIndex="currentstep"
                        key="currentstep"
                        // responsive={['md']}

                        render={(currentstep) => (
                            <>{currentstep}</>
                        )}
                    />
                    <Column title="Status" dataIndex="status" key="status"
                                            // responsive={['md']}

                        render={status => (
                            <>
                                {status.map((tag: any) => (
                                    tag === "revise" ?
                                        <Tag color="orange" key={tag}>
                                            {tag}
                                        </Tag> :
                                        tag === "on-finalisasi" ?
                                            <Tag color="cyan" key={tag}>
                                                {tag}
                                            </Tag> :
                                            <Tag color="red" key={tag}>
                                                {tag}
                                            </Tag>
                                ))}
                            </>
                        )}

                    />
                        <Column
                        title="Remarks"
                        dataIndex="remarks"
                        key="remarks"
                        // responsive={['md']}

                        render={(remarks) => (
                            <>{remarks}</>
                        )}
                    />
                    <Column
                        title=""
                        dataIndex="detail"
                        key="detail"
                        // responsive={['md']}

                        render={detail => (
                            <>
                                {<a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF' }}>{detail}</Text></a>}
                            </>
                        )}
                    />
                </Table>
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
                </Dropdown>
            </Card>
        </div >
    )
}

export default TeamAppraisalComp
