import {
    Card, Row, Col, Image, Button, Divider, PageHeader, Input, Menu, DatePicker, Dropdown, Typography, Tabs, Form, Steps, message, Collapse,
} from 'antd';
import { useState } from 'react';

import IconBackArrow from '../../../assets/icons/icon_back_arrow.svg';
import IconDownload from '../../../assets/icons/icon_download.svg';
import IconPrint from '../../../assets/icons/icon_print.svg';
import IconSubmitCheck from '../../../assets/icons/icon_submit_check.svg';
import IconAdd from '../../../assets/icons/icon_add.svg';
import IconEdit from '../../../assets/icons/icon_edit.svg';
import IconDelete from '../../../assets/icons/icon_delete.svg'
import { DownOutlined, } from '@ant-design/icons';

const { Step } = Steps;
const { Panel } = Collapse;

function callback(key: any) {
    console.log(key);
}
const editCollapse = () => (
    console.log("edit clicked")
);
const deleteCollapse = () => (
    console.log("deleteCollapse clicked")
);

const menuKamus = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                Action 1
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                Action 2
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Action 3
            </a>
        </Menu.Item>
    </Menu>
);
const steps = [
    {
        title: 'Plan',
        titlecontent: 'KPI - 100% Complete',
        content: 'Plan',
        description: "Training Duration: 3 days",
        status: "Completed"
    },
    {
        title: 'Mid Year Review',
        titlecontent: 'Mid Year Review',
        content: 'Mid Year Review',
        description: "Course",
        status: "60% Completed"
    },
    {
        title: 'End Year Review',
        titlecontent: 'End Year Review',
        content: 'End Year Review',
        description: "How to Close your Sales",
        status: ""

    },
    {
        title: 'Sign-Off',
        titlecontent: 'Sign-Off',
        content: 'Sign-Off',
        description: "Maintaining Leads and How to make persistence happens. ",
        status: ""
    },
];

const { RangePicker } = DatePicker;
const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select date!' }],
};

//button icon handle
const onSearch = (value: any) => console.log(value);

const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}
const handleAdd = (value: any) => {
    console.log("add KPI % complete");
}


function KpiPlanSubmissionComp() {
    const { Search } = Input;
    const { TabPane } = Tabs;
    const { Text } = Typography;
    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-period'];
        const values = {
            ...fieldsValue,
            'range-period': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

        };
        console.log('Received values of form: ', values);
    };

    const [current, setCurrent] = useState(0);

    const next = () => {
        setCurrent(current + 1);
    };

    const prev = () => {
        setCurrent(current - 1);
    };


    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
            <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="KPI Plan Submission"
                        extra={[
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload" /></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint" /></Button>

                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>


                <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
                    <Form name="time_related_controls" onFinish={onFinish}>

                        <Row justify="start" align="middle">
                            <Col span={8}>
                                <Form.Item labelCol={{ span: 24 }}
                                    name="range-period" label="Period" {...rangeConfig}>
                                    <RangePicker />
                                </Form.Item>
                            </Col>
                            <Col span={2}>
                                <Search placeholder="Search" onSearch={onSearch} bordered={false} className="searchKpi" style={{ borderStyle: 'none', marginTop: 15 }} />
                            </Col>
                            <Col span={14}>
                            </Col>
                        </Row>
                        <Tabs defaultActiveKey="trainingteknis" tabBarGutter={100} >
                            <TabPane tab="Plan" key="plan">

                                <Card style={{ marginTop: 39, marginBottom: 20, marginLeft: 5, padding: '10px' }} className="listCardKpi" bordered={false}>
                                    <div >
                                        <Row>
                                            <Steps current={current} >
                                                {steps.map(item => (
                                                    <Step key={item.title} title={item.title} style={{ paddingRight: 100, marginBottom: 30 }} />
                                                ))}
                                            </Steps>
                                        </Row>
                                        <Row>
                                            <div className="steps-content" style={{ width: "100%", }}>
                                                <Row justify="space-between" className="listCardKpiInside" align="middle">
                                                    <Col style={{ color: "#009ADA", fontWeight: 500, fontSize: 16.5, }}>
                                                        {steps[current].titlecontent}
                                                    </Col>
                                                    <Col >
                                                        <Row>
                                                            <Col>
                                                                <Dropdown overlay={menuKamus} placement="bottomRight" arrow >
                                                                    <Button>Action<DownOutlined style={{ color: 'black', marginLeft: 30 }} /></Button>
                                                                </Dropdown>
                                                            </Col>
                                                            <Col>
                                                                <Button type="link" onClick={handleAdd}><Image src={IconAdd} preview={false} height={26} /></Button>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>

                                                <Row justify="start" style={{ textAlign: "left", color: '#707070', width: "100%", marginTop: 20 }}>
                                                    {
                                                        steps[current].content === "Plan" ? (
                                                            <>
                                                                <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                    expandIconPosition="left"
                                                                // expandIcon={}
                                                                >
                                                                    <Panel header="Core System Project - 60%" key="1" extra={
                                                                        <Row>
                                                                            <Col style={{ marginRight: 10 }}>

                                                                                <img alt="homeicons" src={IconEdit} height={25} onClick={editCollapse} />
                                                                            </Col>
                                                                            <Col>
                                                                                <img alt="homeicons" src={IconDelete} height={25} onClick={deleteCollapse} />

                                                                            </Col>
                                                                        </Row>}>

                                                                        <Row>
                                                                            <Col span={11}>

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Type"
                                                                                    name="type"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Name"
                                                                                    name="name"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Measurement Data Source"
                                                                                    name="measurementdatasource"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Benefit To Organisation"
                                                                                    name="benefittoorganisation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Timeline"
                                                                                    name="timeline"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Dependancy"
                                                                                    name="dependancy"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Challenges"
                                                                                    name="challenges"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk"
                                                                                    name="risk"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk Mitigation"
                                                                                    name="riskmitigation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                            <Col span={1}></Col>
                                                                            <Col span={12} style={{ backgroundColor: "#F4FEFF", paddingLeft: 14, paddingRight: 14 }}>
                                                                                <h3 style={{ color: "#009ADA", marginTop: 27, fontSize: 18, fontWeight: 500 }}>Target</h3>
                                                                                <Divider style={{ border: '1px solid #009ADA', backgroundColor: '#5067AA', margin: 0, padding: 0, marginBottom: 30 }} />

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Duration To Complete"
                                                                                    name="durationtocomplete"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of minor bugs (1-2 hours)"
                                                                                    name="numberofminorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of major bugs (2 hours - 1 days)"
                                                                                    name="numberofmajorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of severe bugs (showstopper)"
                                                                                    name="numberofseverebugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Weight"
                                                                                    name="weight"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                        </Row>
                                                                    </Panel>
                                                                </Collapse>
                                                                <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                    expandIconPosition="left"
                                                                // expandIcon={}
                                                                >
                                                                    <Panel header="Core System Project - 60%" key="1" extra={
                                                                        <Row>
                                                                            <Col style={{ marginRight: 10 }}>

                                                                                <img alt="homeicons" src={IconEdit} height={25} onClick={editCollapse} />
                                                                            </Col>
                                                                            <Col>
                                                                                <img alt="homeicons" src={IconDelete} height={25} onClick={deleteCollapse} />

                                                                            </Col>
                                                                        </Row>}>

                                                                        <Row>
                                                                            <Col span={11}>

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Type"
                                                                                    name="type"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Name"
                                                                                    name="name"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Measurement Data Source"
                                                                                    name="measurementdatasource"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Benefit To Organisation"
                                                                                    name="benefittoorganisation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Timeline"
                                                                                    name="timeline"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Dependancy"
                                                                                    name="dependancy"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Challenges"
                                                                                    name="challenges"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk"
                                                                                    name="risk"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk Mitigation"
                                                                                    name="riskmitigation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                            <Col span={1}></Col>
                                                                            <Col span={12} style={{ backgroundColor: "#F4FEFF", paddingLeft: 14, paddingRight: 14 }}>
                                                                                <h3 style={{ color: "#009ADA", marginTop: 27, fontSize: 18, fontWeight: 500 }}>Target</h3>
                                                                                <Divider style={{ border: '1px solid #009ADA', backgroundColor: '#5067AA', margin: 0, padding: 0, marginBottom: 30 }} />

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Duration To Complete"
                                                                                    name="durationtocomplete"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of minor bugs (1-2 hours)"
                                                                                    name="numberofminorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of major bugs (2 hours - 1 days)"
                                                                                    name="numberofmajorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of severe bugs (showstopper)"
                                                                                    name="numberofseverebugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Weight"
                                                                                    name="weight"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                        </Row>
                                                                    </Panel>
                                                                </Collapse>
                                                            </>
                                                        ) : (
                                                            <p>No data found</p>
                                                        )
                                                    }
                                                </Row>
                                                <Row justify="start" style={{ textAlign: "left", color: '#707070' }}>

                                                </Row>
                                            </div>

                                        </Row>
                                        <div className="steps-action">
                                            {current < steps.length - 1 && (
                                                <Button type="primary" onClick={() => next()}>
                                                    Next
                                                </Button>
                                            )}
                                            {current === steps.length - 1 && (
                                                <Button type="primary" onClick={() => message.success('Processing complete!')}>
                                                    Done
                                                </Button>
                                            )}
                                            {current > 0 && (
                                                <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                                                    Previous
                                                </Button>
                                            )}
                                        </div>
                                    </div>
                                </Card>
                            </TabPane>
                            {/* <TabPane tab="Review" key="review">

                            </TabPane> */}
                            <TabPane tab="History" key="history">

                            </TabPane>
                        </Tabs>

                        <Form.Item
                            labelCol={{ span: 24 }}
                        >
                            <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                <Row align="middle" justify="start">
                                    <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                    <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                                </Row>
                            </Button>
                        </Form.Item>
                    </Form>

                </Card>
            </Card>

        </div >
    )
}

export default KpiPlanSubmissionComp
