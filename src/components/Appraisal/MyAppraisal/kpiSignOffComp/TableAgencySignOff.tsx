import React, { useState } from 'react';
import { Table, Input, InputNumber, Popconfirm, Form, Typography } from 'antd';

interface Item {
  key: string;
  measurement: string;
  target: string;
  midyear: string;
  endyear: string;
  progressnotes: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: 'number' | 'text';
  record: Item;
  index: number;
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

const TableAgencySignOff = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([
    {
      key: "1",
      measurement: "Duration To Complete",
      target: "85 mandays",
      midyear: "90 mandays",
      endyear: "90 mandays",
      progressnotes: "Slower due to lack of resources"
    },
    {
      key: "2",
      measurement: "Number of minor bugs",
      target: "<5%",
      midyear: "<5%",
      endyear: "3%",
      progressnotes: "-"
    },
    {
      key: "3",
      measurement: "Number of major bugs",
      target: "<1%",
      midyear: "<1%",
      endyear: "0%",
      progressnotes: "-"
    },
    {
      key: "4",
      measurement: `Number of severe bugs`,
      target: "0%",
      midyear: "0%",
      endyear: "0%",
      progressnotes: "-"
    },
  ]);
  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record: Item) => record.key === editingKey;

  const edit = (record: Partial<Item> & { key: React.Key }) => {
    form.setFieldsValue({ measurement: '', target: '', midyear: '', endyear: '',
    progressnotes: '', ...record });
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key: React.Key) => {
    try {
      const row = (await form.validateFields()) as Item;

      const newData = [...data];
      const index = newData.findIndex(item => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
      }
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const columns = [
    {
      title: 'Measurement',
      dataIndex: 'measurement',
      width: '25%',
      editable: true,
    },
    {
      title: 'Target',
      dataIndex: 'target',
      width: '15%',
      editable: true,
    },
    {
      title: 'Mid Year',
      dataIndex: 'midyear',
      width: '15%',
      editable: true,
    },
    {
      title: 'End Year',
      dataIndex: 'endyear',
      width: '15%',
      editable: true,
    },
    {
      title: 'Progress Notes',
      dataIndex: 'progressnotes',
      width: '25%',
      editable: true,
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: (_: any, record: Item) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <p onClick={() => save(record.key)} style={{ marginRight: 8 }}>
              Save
            </p>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <p>Cancel</p>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
            Edit
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: cancel,
        }}
        id="trheader"
      />
    </Form>
  );
};
export default TableAgencySignOff
