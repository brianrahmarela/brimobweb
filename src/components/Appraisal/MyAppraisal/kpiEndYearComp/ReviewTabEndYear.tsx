import {
  Card, Row, Col, Image, Button, Menu, Dropdown, Steps, message, Collapse, Tag, Table, Space
} from 'antd';

// import React, { useContext, useState, useEffect, useRef } from 'react';
import React, { useState, } from 'react';


import IconAvatarKpi from '../../../../assets/icons/icon_avatar_kpi.svg';

import IconAdd from '../../../../assets/icons/icon_add.svg';
import {
  DownOutlined, CheckCircleOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';

//component
import TableCoreSystem from '../kpiMidYearComp/TableCoreSystem';
import TableCoreReviewEnd from '../kpiEndYearComp/TableCoreReviewEnd';
import TableAgency from '../kpiMidYearComp/TableAgency';
import TableAgencyReviewEnd from '../kpiEndYearComp/TableAgencyReviewEnd';
import TableInternal from '../kpiMidYearComp/TableInternalComp';
import TableInternalReviewEnd from '../kpiEndYearComp/TableInternalReviewEnd';

const { Step } = Steps;
const { Panel } = Collapse;
// const { Column, } = Table;

function callback(key: any) {
  console.log(key);
}

const menuKamus = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Action 1
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Action 2
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        Action 3
      </a>
    </Menu.Item>
  </Menu>
);

const menuUpdateHistory = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        1st menuUpdateHistory
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        2st menuUpdateHistory
      </a>
    </Menu.Item>

  </Menu>
);
const steps = [
  {
    title: 'Plan',
    titlecontent: 'KPI - 100% Complete',
    content: 'Plan',
    description: "Training Duration: 3 days",
    status: "Completed"
  },
  {
    title: 'Mid Year Review',
    titlecontent: 'KPI - 100% Complete',
    content: 'Mid Year Review',
    description: "Course",
    status: "60% Completed"
  },
  {
    title: 'End Year Review',
    titlecontent: 'KPI - 100% Complete',
    content: 'End Year Review',
    description: "How to Close your Sales",
    status: ""

  },
  {
    title: 'Sign-Off',
    titlecontent: 'Sign-Off',
    content: 'Sign-Off',
    description: "Maintaining Leads and How to make persistence happens. ",
    status: ""
  },
];

const handleAdd = (value: any) => {
  console.log("add KPI % complete");
}

const dataSource = [
  {
    key: '1',
    reviewer: 'You',
    totalkpiscore: 32,
    grade: 'A',
  },

];

const columns = [
  {
    title: 'Reviewer',
    dataIndex: 'reviewer',
    key: 'reviewer',
  },
  {
    title: 'Total KPI Score',
    dataIndex: 'totalkpiscore',
    key: 'totalkpiscore',
  },
  {
    title: 'Grade',
    dataIndex: 'grade',
    key: 'grade',
  },
];

function ReviewTabEndYear() {
  const [size] = useState(6);

  const [current, setCurrent] = useState(0);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };


  return (
    <Card style={{ marginTop: 39, marginBottom: 20, marginLeft: 5, padding: '10px' }} className="listCardKpi" bordered={false}>
      <div >
        <Row>
          <Steps current={current} >
            {steps.map(item => (
              <Step key={item.title} title={item.title} style={{ paddingRight: 100, marginBottom: 30 }} />
            ))}
          </Steps>
        </Row>
        <Row>
          <div className="steps-content" style={{ width: "100%", }}>
            <Row justify="space-between" className="listCardKpiInside" align="middle">
              <Col style={{ color: "#009ADA", fontWeight: 500, fontSize: 16.5, }}>
                {steps[current].titlecontent}
              </Col>
              <Col >
                <Row>
                  <Col>
                    <Dropdown overlay={menuKamus} placement="bottomRight" arrow >
                      <Button>Action<DownOutlined style={{ color: 'black', marginLeft: 30 }} /></Button>
                    </Dropdown>
                  </Col>
                  <Col>
                    <Button type="link" onClick={handleAdd}><Image src={IconAdd} preview={false} height={26} /></Button>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row justify="start" style={{ textAlign: "left", color: '#707070', width: "100%", marginTop: 20 }}>

              {
                steps[current].content === "Plan" ? (
                  <>
                    <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Core System Project - 60%" key="1" >
                        <TableCoreSystem />
                        <Dropdown overlay={menuUpdateHistory}>
                          <p className="ant-dropdown-link" onClick={e => e.preventDefault()} style={{ marginLeft: 15, fontSize: 17, color: 'black', fontWeight: 600 }}>
                            Updates History<DownOutlined />
                          </p>
                        </Dropdown>
                        <Card bordered={false} className="listCardKpiApproval">
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<CheckCircleOutlined />} color="success" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The audit finding is still on tracked
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<ExclamationCircleOutlined />} color="warning" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The BRD are not fully capture user Requirement
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                        </Card>
                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['2']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Agency Admin Enhancement Project - 20%" key="2" >
                        <TableAgency />
                        {/* <Table dataSource={data} columns={columns} /> */}

                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['3']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Internal Audit - 20%" key="3" >
                        <TableInternal />
                      </Panel>
                    </Collapse>
                  </>
                ) : steps[current].content === "Mid Year Review" ? (
                  <>
                    <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Core System Project - 60%" key="1" >
                        <TableCoreSystem />
                        <Dropdown overlay={menuUpdateHistory}>
                          <p className="ant-dropdown-link" onClick={e => e.preventDefault()} style={{ marginLeft: 15, fontSize: 17, color: 'black', fontWeight: 600 }}>
                            Updates History<DownOutlined />
                          </p>
                        </Dropdown>
                        <Card bordered={false} className="listCardKpiApproval">
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<CheckCircleOutlined />} color="success" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The audit finding is still on tracked
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<ExclamationCircleOutlined />} color="warning" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The BRD are not fully capture user Requirement
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                        </Card>
                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['2']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Agency Admin Enhancement Project - 20%" key="2" >
                        <TableAgency />
                        {/* <Table dataSource={data} columns={columns} /> */}

                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['3']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Internal Audit - 20%" key="3" >
                        <TableInternal />
                      </Panel>
                    </Collapse>

                  </>
                ) : steps[current].content === "End Year Review" ? (
                  <>
                    <Card bordered={false} className="listCardNoBg">
                      <Row justify="space-between" style={{ marginBottom: 20 }} >
                        <Col  lg={24} xl={11}>
                          <Table dataSource={dataSource} columns={columns} id="trheader"/>

                        </Col>
                        <Col lg={0} xl={1}></Col>
                        <Col  lg={24} xl={12} >
                          <Row justify="start">
                            <Col lg={12} xl={12}>
                              <Tag color="cyan" style={{ padding: "25px 50px 27px 20px", marginRight: 15 }}>
                                <Row style={{ fontSize: 18, fontWeight: 600, marginBottom: 18,  }}>
                                  KPI Final Score
                                </Row>
                                <Space size={size} direction="vertical">
                                  <Row justify="space-between" style={{ fontSize: 13.5 }}>
                                    <Space size={50}>

                                      <Col >Number of KPI</Col>
                                      <Col >3</Col>
                                    </Space>
                                  </Row>
                                  <Row justify="space-between" style={{ fontSize: 13.5 }}>

                                    <Col>Total KPI Score</Col>

                                    <Col>4.2</Col>
                                  </Row>
                                  <Row justify="space-between" style={{ fontSize: 13.5 }}>
                                    <Col>Grade</Col>

                                    <Col>A</Col>
                                  </Row>
                                </Space>
                              </Tag>
                              {/* <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} /> */}
                            </Col>
                            <Col lg={12} xl={12}>
                              <Card className="listCardKpi">

                                <Row justify="center" style={{ fontSize: 100, fontWeight: 600, color: "darkcyan", lineHeight: 1.1 }}>
                                  B
                                </Row>
                                <Row justify="center" style={{ color: "darkcyan" }}>
                                  <Row style={{ fontSize: 18, fontWeight: 400 }}>4.2</Row>

                                </Row>
                              </Card>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Card>
                    <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                      style={{ marginTop: 20 }}>
                      <Panel header="Core System Project - 60%" key="1" >

                        <TableCoreReviewEnd />
                        <Card style={{ backgroundColor: "#e6fffb", padding: 20, marginTop: 20, marginBottom: 30 }} bordered={false}>
                          <Row style={{ marginLeft: 4, marginBottom: 20, fontSize: 19, color: 'darkcyan', fontWeight: 600 }}>
                            Scores
                          </Row>
                          <Space size={17} direction="vertical">
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Space size={100}>

                                <Col >Score</Col>
                                <Col >4</Col>
                              </Space>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>

                              <Col>Weight</Col>

                              <Col>60%</Col>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Col>Final Score</Col>

                              <Col>2.4</Col>
                            </Row>
                          </Space>
                        </Card>
                        <Dropdown overlay={menuUpdateHistory}>
                          <p className="ant-dropdown-link" onClick={e => e.preventDefault()} style={{ marginLeft: 15, fontSize: 17, color: 'black', fontWeight: 600 }}>
                            Updates History<DownOutlined />
                          </p>
                        </Dropdown>
                        <Card bordered={false} className="listCardKpiApproval">
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<CheckCircleOutlined />} color="success" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The audit finding is still on tracked
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                          <Row justify="space-between" style={{ marginBottom: 20 }} align="middle">
                            <Col md={4} lg={2} xl={2} >
                              <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={47} />
                            </Col>
                            <Col md={1} lg={1} xl={1}></Col>
                            <Col md={23} lg={21} xl={21}>
                              <Row justify="space-between">
                                <Col style={{ fontSize: 15, fontWeight: 500, marginBottom: 5 }}>You</Col>
                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                              </Row>
                              <Row style={{ color: '#757575' }}>
                                <Tag icon={<ExclamationCircleOutlined />} color="warning" style={{ fontSize: 14, padding: "4px 10px 4px 10px" }}>
                                  The BRD are not fully capture user Requirement
                                </Tag>
                              </Row>
                            </Col>
                          </Row>
                        </Card>
                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['2']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Agency Admin Enhancement Project - 20%" key="2" >
                        <TableAgencyReviewEnd />
                        <Card style={{ backgroundColor: "#e6fffb", padding: 20, marginTop: 20, marginBottom: 30 }} bordered={false}>
                          <Row style={{ marginLeft: 4, marginBottom: 20, fontSize: 19, color: 'darkcyan', fontWeight: 600 }}>
                            Scores
                          </Row>
                          <Space size={17} direction="vertical">
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Space size={100}>

                                <Col >Score</Col>
                                <Col >5</Col>
                              </Space>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>

                              <Col>Weight</Col>

                              <Col>20%</Col>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Col>Final Score</Col>

                              <Col>1.0</Col>
                            </Row>
                          </Space>
                        </Card>
                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['3']} onChange={callback} bordered={false} className="cardKpiCollapse"
                    >
                      <Panel header="Internal Audit - 20%" key="3" >
                        <TableInternalReviewEnd />
                        <Card style={{ backgroundColor: "#e6fffb", padding: 20, marginTop: 20, marginBottom: 30 }} bordered={false}>
                          <Row style={{ marginLeft: 4, marginBottom: 20, fontSize: 19, color: 'darkcyan', fontWeight: 600 }}>
                            Scores
                          </Row>
                          <Space size={17} direction="vertical">
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Space size={100}>

                                <Col >Score</Col>
                                <Col >4</Col>
                              </Space>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>

                              <Col>Weight</Col>

                              <Col>20%</Col>
                            </Row>
                            <Row justify="space-between" style={{ fontSize: 14, marginLeft: 4, }}>
                              <Col>Final Score</Col>

                              <Col>0.8</Col>
                            </Row>
                          </Space>
                        </Card>
                      </Panel>
                    </Collapse>

                  </>
                ) : (
                  <p>No data found</p>
                )
              }
            </Row>
            <Row justify="start" style={{ textAlign: "left", color: '#707070' }}>
            </Row>
          </div>
        </Row>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => message.success('Processing complete!')}>
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
              Previous
            </Button>
          )}
        </div>
      </div>
    </Card>

  )
}

export default ReviewTabEndYear

