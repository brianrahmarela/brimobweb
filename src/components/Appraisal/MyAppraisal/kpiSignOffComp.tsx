import {
    Card, Row, Col, Image, Button, Divider, Input, PageHeader, Menu, DatePicker, Dropdown, Typography, Tabs, Form, Steps, message, Collapse, Space, Table, Upload
} from 'antd';

// import React, { useContext, useState, useEffect, useRef } from 'react';
import React, { useState, } from 'react';

import IconBackArrow from '../../../assets/icons/icon_back_arrow.svg';
import IconDownload from '../../../assets/icons/icon_download.svg';
import IconPrint from '../../../assets/icons/icon_print.svg';
import IconSubmitCheck from '../../../assets/icons/icon_submit_check.svg';
import IconUndo from '../../../assets/icons/icon_undo.svg';
import IconAvatarKpi from '../../../assets/icons/icon_avatar_kpi.svg';

import IconAdd from '../../../assets/icons/icon_add.svg';
import { DownOutlined, UploadOutlined } from '@ant-design/icons';

//component
import InternalAuditComp from './kpiApprovalComp/InternalAuditComp';
import ReviewTabSignOff from './kpiSignOffComp/ReviewTabSignOff';

const props = {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
        authorization: 'authorization-text',
    },
    onChange(info: any) {
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    },
};

const { Step } = Steps;
const { Panel } = Collapse;

function callback(key: any) {
    console.log(key);
}
const editCollapse = () => (
    console.log("add clicked")
);


const columns = [
    {
        title: 'Name',
        dataIndex: 'measurement',
        key: 'measurement',
    },
    {
        title: 'target',
        dataIndex: 'target',
        key: 'target',

    },
    {
        title: 'Mid Year Actual',
        dataIndex: 'midyearactual',
        key: 'midyearactual',
        render: () => {
            return <Form.Item labelCol={{ span: 24 }}
                name="midyearactual" label="">
                <Input />
            </Form.Item>
        } 

    },
    {
        title: 'Progress Notes',
        dataIndex: 'progressnotes',
        key: 'progressnotes',
        render: () => {
            return <Form.Item labelCol={{ span: 24 }}
                name="progressnotes" label="">
                <Input />
            </Form.Item>
        }
    },
];

const menuKamus = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                Action 1
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                Action 2
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Action 3
            </a>
        </Menu.Item>
    </Menu>
);

const menuUpdateHistory = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menuUpdateHistory
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                2st menuUpdateHistory
            </a>
        </Menu.Item>

    </Menu>
);
const steps = [
    {
        title: 'Plan',
        titlecontent: 'KPI - 100% Complete',
        content: 'Plan',
        description: "Training Duration: 3 days",
        status: "Completed"
    },
    {
        title: 'Mid Year Review',
        titlecontent: '100% Complete',
        content: 'Mid Year Review',
        description: "Course",
        status: "60% Completed"
    },
    {
        title: 'End Year Review',
        titlecontent: 'End Year Review',
        content: 'End Year Review',
        description: "How to Close your Sales",
        status: ""

    },
    {
        title: 'Sign-Off',
        titlecontent: 'Sign-Off',
        content: 'Sign-Off',
        description: "Maintaining Leads and How to make persistence happens. ",
        status: ""
    },
];

const { RangePicker } = DatePicker;
const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select date!' }],
};

//button icon handle
const onSearch = (value: any) => console.log(value);

const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}
const handleAdd = (value: any) => {
    console.log("add KPI % complete");
}


function KpiSignOffComp() {

    const [data] = useState([

        {
            key: '1',
            measurement: 'Duration To Complete',
            target: '85 mandays',
            midyearactual: '24 Juli 2021 15:00',
            progressnotes: 'on-progress',
        },
        {
            key: '2',
            measurement: 'Number of minor bugs',
            target: '<5%',
            midyearactual: '2 Juli 2021 15:00',
            progressnotes: 'completed',
        },
        {
            key: '3',
            measurement: 'Number of major bugs',
            target: '<1%',
            midyearactual: '22 Juli 2021 15:00',
            progressnotes: 'completed',
        },
        {
            key: '4',
            measurement: 'Number of severe bugs',
            target: '0%',
            midyearactual: '22 Juli 2021 15:00',
            progressnotes: 'completed',
        },

    ])
    // const [expandIconPosition] = useState("");
    const [size] = useState(12);

    const { Search } = Input;
    const { TabPane } = Tabs;
    const { Text } = Typography;
    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-period'];
        const values = {
            ...fieldsValue,
            'range-period': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

        };
        console.log('Received values of form: ', values);
    };

    const [current, setCurrent] = useState(0);

    const next = () => {
        setCurrent(current + 1);
    };

    const prev = () => {
        setCurrent(current - 1);
    };


    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="KPI Sign Off"
                        extra={[
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload" /></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint" /></Button>

                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>


                <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
                    <Form name="time_related_controls" onFinish={onFinish}
                    >

                        <Row justify="start" align="middle">
                            <Col span={8}>
                                <Form.Item labelCol={{ span: 24 }}
                                    name="range-period" label="Period" {...rangeConfig}>
                                    <RangePicker />
                                </Form.Item>
                            </Col>
                            <Col >
                                <Search placeholder="Search" onSearch={onSearch} bordered={false} className="searchKpi" style={{ borderStyle: 'none', marginTop: 15 }} />
                            </Col>
                        </Row>
                        <Tabs defaultActiveKey="trainingteknis" tabBarGutter={100} >
                            <TabPane tab="Plan" key="plan">

                                <Card style={{ marginTop: 39, marginBottom: 20, marginLeft: 5, padding: '10px' }} className="listCardKpi" bordered={false}>
                                    <div >
                                        <Row>
                                            <Steps current={current} >
                                                {steps.map(item => (
                                                    <Step key={item.title} title={item.title} style={{ paddingRight: 100, marginBottom: 30 }} />
                                                ))}
                                            </Steps>
                                        </Row>
                                        <Row>
                                            <div className="steps-content" style={{ width: "100%", }}>
                                                <Row justify="space-between" className="listCardKpiInside" align="middle">
                                                    <Col style={{ color: "#009ADA", fontWeight: 500, fontSize: 16.5, }}>
                                                        {steps[current].titlecontent}
                                                    </Col>
                                                    <Col >
                                                        <Row>
                                                            <Col>
                                                                <Dropdown overlay={menuKamus} placement="bottomRight" arrow >
                                                                    <Button>Action<DownOutlined style={{ color: 'black', marginLeft: 30 }} /></Button>
                                                                </Dropdown>
                                                            </Col>
                                                            <Col>
                                                                <Button type="link" onClick={handleAdd}><Image src={IconAdd} preview={false} height={26} /></Button>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>

                                                <Row justify="start" style={{ textAlign: "left", color: '#707070', width: "100%", marginTop: 20 }}>

                                                    {
                                                        steps[current].content === "Plan" ? (
                                                            <>
                                                                <Collapse defaultActiveKey={['1']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                >
                                                                    <Panel header="Core System Project - 60%" key="1" >
                                                                        <Row>
                                                                            <Col span={11}>

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Type"
                                                                                    name="type"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Name"
                                                                                    name="name"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Measurement Data Source"
                                                                                    name="measurementdatasource"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Benefit To Organisation"
                                                                                    name="benefittoorganisation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Timeline"
                                                                                    name="timeline"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Dependancy"
                                                                                    name="dependancy"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Challenges"
                                                                                    name="challenges"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk"
                                                                                    name="risk"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Risk Mitigation"
                                                                                    name="riskmitigation"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                            <Col span={1}></Col>
                                                                            <Col span={12} style={{ backgroundColor: "#F4FEFF", paddingLeft: 14, paddingRight: 14 }}>
                                                                                <h3 style={{ color: "#009ADA", marginTop: 27, fontSize: 18, fontWeight: 500 }}>Target</h3>
                                                                                <Divider style={{ border: '1px solid #009ADA', backgroundColor: '#5067AA', margin: 0, padding: 0, marginBottom: 30 }} />

                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Duration To Complete"
                                                                                    name="durationtocomplete"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of minor bugs (1-2 hours)"
                                                                                    name="numberofminorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of major bugs (2 hours - 1 days)"
                                                                                    name="numberofmajorbugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Number of severe bugs (showstopper)"
                                                                                    name="numberofseverebugs"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                                <Form.Item
                                                                                    labelCol={{ span: 24 }}
                                                                                    label="Weight"
                                                                                    name="weight"
                                                                                >
                                                                                    <Input />
                                                                                </Form.Item>
                                                                            </Col>
                                                                        </Row>
                                                                    </Panel>
                                                                </Collapse>
                                                                <Collapse defaultActiveKey={['2']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                >
                                                                    <Panel header="Agency Admin Enhancement Project - 20%" key="2" >
                                                                        <Table dataSource={data} columns={columns} id="trheader" />

                                                                    </Panel>
                                                                </Collapse>
                                                                <Collapse defaultActiveKey={['3']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                >
                                                                    <Panel header="Internal Audit - 20%" key="3" >
                                                                        <InternalAuditComp />
                                                                    </Panel>
                                                                </Collapse>
                                                                <Collapse defaultActiveKey={['4']} onChange={callback} bordered={false} className="cardKpiCollapse"
                                                                >
                                                                    <Panel header="Notes" key="1" extra={
                                                                        <Row>

                                                                            <img alt="homeicons" src={IconAdd} height={25} onClick={editCollapse} />


                                                                        </Row>}>

                                                                        <Form.Item
                                                                            labelCol={{ span: 24 }}
                                                                            label="Notes"
                                                                            name="notes"
                                                                        >
                                                                            <Input />
                                                                        </Form.Item>

                                                                        <Form.Item
                                                                            labelCol={{ span: 24 }}
                                                                            label="Attachment"
                                                                            name="attachment"
                                                                        >
                                                                            <Upload {...props}>
                                                                                <Button icon={<UploadOutlined />}>Click to Upload</Button>
                                                                            </Upload>
                                                                        </Form.Item>

                                                                    </Panel>
                                                                </Collapse>

                                                                <Dropdown overlay={menuUpdateHistory}>
                                                                    <p className="ant-dropdown-link" onClick={e => e.preventDefault()} style={{ marginLeft: 15, fontSize: 17, color: 'black', fontWeight: 600 }}>
                                                                        Updates History<DownOutlined />
                                                                    </p>
                                                                </Dropdown>
                                                                <Card bordered={false} className="listCardKpiApproval">
                                                                    <Row justify="space-between">
                                                                        <Col md={4} lg={2} xl={2} >
                                                                            <Image src={IconAvatarKpi} preview={false} alt="iconavatarkpi" height={45} />

                                                                        </Col>
                                                                        <Col md={1} lg={1} xl={1}></Col>
                                                                        <Col md={23} lg={21} xl={21}>
                                                                            <Row justify="space-between">
                                                                                <Col style={{ fontSize: 15, fontWeight: 500 }}>You</Col>
                                                                                <Col style={{ color: '#757575' }}>4:09 PM</Col>
                                                                            </Row>
                                                                            <Row style={{ color: '#757575' }}>
                                                                                Please Change Core System Project Weight to 70%
                                                                            </Row>
                                                                        </Col>
                                                                    </Row>
                                                                </Card>

                                                            </>
                                                        ) : (
                                                            <p>No data found</p>
                                                        )
                                                    }
                                                </Row>
                                                <Row justify="start" style={{ textAlign: "left", color: '#707070' }}>

                                                </Row>
                                            </div>

                                        </Row>
                                        <div className="steps-action">
                                            {current < steps.length - 1 && (
                                                <Button type="primary" onClick={() => next()}>
                                                    Next
                                                </Button>
                                            )}
                                            {current === steps.length - 1 && (
                                                <Button type="primary" onClick={() => message.success('Processing complete!')}>
                                                    Done
                                                </Button>
                                            )}
                                            {current > 0 && (
                                                <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                                                    Previous
                                                </Button>
                                            )}
                                        </div>
                                    </div>
                                </Card>
                            </TabPane>
                            <TabPane tab="Review" key="review">
                                <ReviewTabSignOff />
                            </TabPane>
                            <TabPane tab="History" key="history">
                            </TabPane>
                        </Tabs>

                        <Form.Item
                            labelCol={{ span: 24 }}
                        >
                            <Row>
                                <Space size={size}>

                                    <Col>
                                        <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                            <Row align="middle" justify="start">
                                                <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                                <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                                            </Row>
                                        </Button>
                                    </Col>
                                    <Col>
                                        <Button type="primary" htmlType="reset" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                            <Row align="middle" justify="start">
                                                <Image src={IconUndo} preview={false} height={24} style={{ marginRight: 10.4 }} />
                                                <Text style={{ color: '#FFB100', fontSize: 16, }}>Revise</Text>
                                            </Row>
                                        </Button>

                                    </Col>

                                </Space>

                            </Row>
                        </Form.Item>
                    </Form>


                </Card>
            </Card>

        </div >
    )
}

export default KpiSignOffComp

