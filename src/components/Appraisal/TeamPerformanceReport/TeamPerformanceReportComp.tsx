import { Card, PageHeader, Button, Divider, Input, Menu, Dropdown, Form, DatePicker } from 'antd';
import IconBackArrow from '../../../assets/icons/icon_back_arrow.svg';
import { DownOutlined } from '@ant-design/icons';
// import IconSubmitCheck from '../../../assets/icons/icon_submit_check.svg';

import TableTeamPerformance from './TableTeamPerformance';

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

//button icon handle
const onSearch = (value: any) => console.log(value);

const { RangePicker } = DatePicker;
const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select date!' }],
};

function TeamPerformanceReportComp() {
    const { Search } = Input;

    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-period'];
        const values = {
            ...fieldsValue,
            'range-period': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],

        };
        console.log('Received values of form: ', values);
    };

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Team Performance Report"

                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
                <Search placeholder="Search Team Member" onSearch={onSearch} bordered={false} className="searchLearning" style={{ padding: 10, marginTop: 39, borderStyle: 'none' }} />
                <Form name="time_related_controls" onFinish={onFinish}
                >
                    <Form.Item labelCol={{ span: 24 }}
                        name="range-period" label="Period" {...rangeConfig}>
                        <RangePicker />
                    </Form.Item>
                    {/* <Form.Item
                        labelCol={{ span: 24 }}
                    >
                        <Row>
                            <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                                <Row align="middle" justify="start">
                                    <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                    <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                                </Row>
                            </Button>
                        </Row>
                    </Form.Item> */}
                </Form>
                <TableTeamPerformance />
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
                </Dropdown>
            </Card>
        </div >
    )
}

export default TeamPerformanceReportComp
