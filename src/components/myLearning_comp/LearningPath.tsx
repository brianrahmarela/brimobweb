import { Card, PageHeader, Table, Button, Divider, Input, Tag, Menu, Dropdown, } from "antd";
import { useState } from "react";

import IconBackArrow from "../../assets/icons/icon_back_arrow.svg";
import { DownOutlined } from "@ant-design/icons";

import { Link, useRouteMatch } from "react-router-dom";

const { Column } = Table;
const menu = (
	<Menu>
		<Menu.Item>
			<a
				target="_blank"
				rel="noopener noreferrer"
				href="https://www.antgroup.com"
			>
				1st menu item
			</a>
		</Menu.Item>
		<Menu.Item>
			<a
				target="_blank"
				rel="noopener noreferrer"
				href="https://www.aliyun.com"
			>
				2nd menu item
			</a>
		</Menu.Item>
		<Menu.Item>
			<a
				target="_blank"
				rel="noopener noreferrer"
				href="https://www.luohanacademy.com"
			>
				3rd menu item
			</a>
		</Menu.Item>
	</Menu>
);

const onSearch = (value: any) => console.log(value);

function LearningPath() {
	let { url } = useRouteMatch();

	const { Search } = Input;

	const [data] = useState([
		{
			key: "1",
			competencyName: "sales",
			NumberOfCourse: "4",
			status: ["on-progress"],
		},
		{
			key: "2",
			competencyName: "People Management",
			NumberOfCourse: "5",
			status: ["completed"],
		},
		{
			key: "3",
			competencyName: "tiga",
			NumberOfCourse: "Brown",
			status: ["on-progress"],
		},
		{
			key: "4",
			competencyName: "empat",
			NumberOfCourse: "Green",
			status: ["completed"],
		},
		{
			key: "5",
			competencyName: "lima",
			NumberOfCourse: "Green",
			status: ["completed"],
		},
		{
			key: "6",
			competencyName: "enam",
			NumberOfCourse: "Brown",
			status: ["on-progress"],
		},
		{
			key: "7",
			competencyName: "tujuh",
			NumberOfCourse: "Brown",
			status: ["on-progress"],
		},
		{
			key: "8",
			competencyName: "delapan",
			NumberOfCourse: "Brown",
			status: ["on-progress"],
		},
		{
			key: "9",
			competencyName: "sembilan",
			NumberOfCourse: "Brown",
			status: ["on-progress"],
		},
		{
			key: "10",
			competencyName: "sepuluh",
			NumberOfCourse: "Green",
			status: ["completed"],
		},
		{
			key: "11",
			competencyName: "sebelas",
			NumberOfCourse: "Green",
			status: ["completed"],
		},
	]);

	return (
		<div className="site-card-wrapper" style={{ marginBottom: 40 }}>
			<Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
				<div className="site-page-header-ghost-wrapper">
					<PageHeader
						ghost={false}
						backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
						onBack={() => window.history.back()}
						title="Learning Path"
					>
					</PageHeader>
					<Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
				</div>
				<Search
					placeholder="Search for Competency"
					onSearch={onSearch}
					bordered={false}
					className="searchLearning"
					style={{ padding: 10, borderStyle: "none" }}
				/>
				<Table dataSource={data}>
					<Column
						title="Competency Name"
						dataIndex="competencyName"
						key="competencyName"
						render={(competency) => (
							<>
								{
									competency === "sales" ? (
										<Link to={`${url}/competencyLearningPath`} style={{ color: "black", fontSize: 13 }} >{competency}</Link>

										// <a href="/competencyLearningPath" style={{color: "black", fontSize: 13}}>{competency}
										// </a>
									) : (
										<a href="/mylearningPath" style={{ color: "black", fontSize: 13 }}>{competency}
										</a>
									)
								}
							</>
						)}
					/>
					<Column
						title="Number Of Course"
						dataIndex="NumberOfCourse"
						key="NumberOfCourse"
						render={(number) => (
							<>
								{
									number === "4" ? (
										<p style={{ color: "black", fontSize: 13 }}>{number}
										</p>
									) : (
										<p style={{ color: "black", fontSize: 13 }}>{number}
										</p>
									)
								}
							</>
						)}
					/>
					<Column
						title="status"
						dataIndex="status"
						key="status"

						render={(status) => (
							<>
								{status.map((tag: any) =>
									tag === "on-progress" ? (
										<Tag color="cyan" key={tag}>
											{tag}
										</Tag>
									) : tag === "completed" ? (
										<Tag color="blue" key={tag}>
											{tag}
										</Tag>
									) : (
										<Tag color="default" key={tag}>
											{tag}
										</Tag>
									)
								)}
							</>
						)}
					/>
				</Table>
				<Dropdown overlay={menu} placement="bottomRight" arrow>
					<Button>
						Sort By Date <DownOutlined style={{ color: "black" }} />
					</Button>
				</Dropdown>
			</Card>
		</div>
	);
}

export default LearningPath;
