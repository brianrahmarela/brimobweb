import { Card, Row, Col, Image, Button, Divider, Steps, message, PageHeader } from "antd";
import { useState } from 'react';

import IconBackArrow from "../../assets/icons/icon_back_arrow.svg";
import IconActiveSales from "../../assets/icons/icon_active_sales.svg";
import { CaretRightOutlined } from '@ant-design/icons';

const { Step } = Steps;

const steps = [
	{
		title: 'Sales for Beginner',
		titlecontent: 'Sales for Beginner',
		content: 'First-content',
		description: "Training Duration: 3 days",
		status: "Completed"
	},
	{
		title: 'Active Sales Strategy',
		titlecontent: 'Active Sales Strategy',
		content: 'By establishing a strategy based on these proven sales principles, you’ll create a culture that values efficiency and empowers reps to do their best work and be properly rewarded for it. An effective sales strategy will also help provide your customers with value at every step of the customer journey',
		description: "Course",
		status: "60% Completed"
	},
	{
		title: 'Closing Sales',
		titlecontent: 'Closing Sales',
		content: 'third-content',
		description: "How to Close your Sales",
		status: ""

	},
	{
		title: 'Maintain Leads',
		titlecontent: 'Maintain Leads',
		content: 'fourth-content',
		description: "Maintaining Leads and How to make persistence happens. ",
		status: ""
	},
];

function CompetencyName() {

	const [current, setCurrent] = useState(0);

	const next = () => {
		setCurrent(current + 1);
	};

	const prev = () => {
		setCurrent(current - 1);
	};

	const ContinueCourse = () => {
		console.log("ContinueCourse ")
	}

	return (
		<div className="site-card-wrapper" style={{ marginBottom: 40 }}>
			<Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
				<div className="site-page-header-ghost-wrapper">
					<PageHeader
						ghost={false}
						backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
						onBack={() => window.history.back()}
						title="Learning Path"
					>
					</PageHeader>
					<Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
				</div>
				<div className="cardlist" >
					<Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
						<Row >
							<h4 style={{ fontSize: 17, fontWeight: 600 }}>Competency - Sales</h4>
						</Row>
						<Row style={{ color: "#606060" }}>
							This competencies are for sales person
						</Row>
						<Row style={{ margin: "30px 0 0 0", fontWeight: 500 }}>
							Last Activity:
						</Row>
						<Row style={{ color: "#606060" }}>
							Course - Active Sales Strategy
						</Row>
						<div style={{ marginTop: 34 }}>
							<h4 style={{ fontSize: 17, fontWeight: 600, color: '#4C62A2', marginBottom: 18 }}>Learning Path</h4>
							<Row>
								<Col span={12} xs={24} md={24} lg={12} xl={12}>
									<Steps current={current} direction="vertical" >
										{steps.map(item => (
											<Step key={item.title} title={item.title}
												description=
												{
													item.status === "Completed" ? (
														<div>
															<Row >{item.description}</Row>
															<Row style={{ color: "#009ADA", fontWeight: 600, fontSize: 14 }}>
																{item.status}
															</Row>
														</div>
													) : (
														<div>
															<Row style={{ fontSize: 13.5 }}>{item.description}</Row><Row >{item.status}</Row>

														</div>
													)
												}
												style={{ paddingBottom: 20 }} />
										))}
									</Steps>
								</Col>
								<Col span={12} xs={24} md={24} lg={12} xl={12}>
									<div className="steps-content">
										<Row justify="center" >
											<Col span={2} style={{ marginRight: 16 }}>
												<Row >
													<Image src={IconActiveSales} preview={false} height={30} />
												</Row>
											</Col>
											<Col span={20}>
												<Row justify="start" >
													<Col style={{ fontSize: 15, margin: "5px 0 5px 0" }}>{steps[current].titlecontent}
													</Col>
												</Row>
												<Row justify="start" style={{ textAlign: "left", color: '#707070' }}>
													<Col style={{ fontSize: 13.5 }}>{steps[current].content}</Col>
												</Row>
												<Row justify="start" style={{ textAlign: "left", color: '#707070' }}>
													<Button type="primary" shape="round" icon={<CaretRightOutlined />} style={{ marginTop: 15, borderRadius: 8, backgroundColor: "#009ADA", borderColor: "#009ADA", fontWeight: 500 }} onClick={ContinueCourse}>
														Continue the course
													</Button>
												</Row>
											</Col>
										</Row>
									</div>
								</Col>
							</Row>
							<div className="steps-action">
								{current < steps.length - 1 && (
									<Button type="primary" onClick={() => next()}>
										Next
									</Button>
								)}
								{current === steps.length - 1 && (
									<Button type="primary" onClick={() => message.success('Processing complete!')}>
										Done
									</Button>
								)}
								{current > 0 && (
									<Button style={{ margin: '0 8px' }} onClick={() => prev()}>
										Previous
									</Button>
								)}
							</div>
						</div>
					</Card>
				</div>
			</Card>
		</div>
	);
}

export default CompetencyName;
