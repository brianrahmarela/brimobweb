import React from 'react'
import { Card, Row, Col, Space, Image, Divider } from 'antd';
import IconCommunication from '../../assets/icons/icon_communication_skill.svg';

function CardSchedule() {
  return (
    <div className="cardlist">
      <Card bordered={false} style={{  padding: "5px 18px 5px 18px" }}>
        <h3 style={{ fontSize: 19, fontWeight: 600, marginBottom: 25.8 }}>Today Schedule: 29 June 2019</h3>
        <Row justify="space-between">

          <Col sm={24} md={24} lg={8} style={{marginBottom: 20}}>
            <Row >
              <Col>
                <Row justify="start">
                <Space size={19} >
                    <Col >
                      <Image src={IconCommunication} preview={false} />
                    </Col>
                    <Col >
                      <Row style={{ fontSize: 14, fontWeight: 600, color: "#4C62A2" }}>
                        Communication Skill
                      </Row>
                      <Row >
                        Kompetensi Manajerial
                      </Row>
                      <Row >
                        10 participant
                      </Row>
                    </Col>
                    </Space>

                </Row>
              </Col>
            </Row>
          </Col>
          <Col sm={24} md={14} lg={8} style={{marginBottom: 20, paddingRight: 20}}>
            <Row >
              <Col>
                <Row justify="start">
                    <Col >
                      <Row>
                        SO Office Bandung
                      </Row>
                      <Row>
                        Jl. Naripan Kavling XIII No 134
                      </Row>
                    
                    </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col sm={24} md={9} lg={8} style={{marginBottom: 20}}>
            <Row  >
              <Col>
                <Row justify="end">
                  <Space size={8} >
                    <Col style={{fontWeight: 500, color: "#4C62A2" }}>
                      <Row>
                        5th May 2019
                      </Row>
                      <Row>
                        17.00 - 20.00 PM
                      </Row>
                    </Col>
                  </Space>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider/>

      </Card>
    </div>
  )
}

export default CardSchedule
