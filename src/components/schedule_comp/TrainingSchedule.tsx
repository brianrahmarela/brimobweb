import { Card, PageHeader, Divider, Calendar, Badge } from "antd";

import CardSchedule from "./CardSchedule";
import IconBackArrow from "../../assets/icons/icon_back_arrow.svg";

function getListData(value: any) {
  let listData: any;
  switch (value.date()) {
    case 2:
      listData = [
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 4:
      listData = [
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 5:
      listData = [
        { type: 'warning', content: 'Training' },
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 14:
      listData = [
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 20:
      listData = [
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 28:
      listData = [
        { type: 'warning', content: 'Training' },
      ];
      break;
    case 29:
      listData = [
        { type: 'warning', content: 'Communication Skill' },
      ];
      break;

    default:
  }
  return listData || [];
}

function dateCellRender(value: any) {
  const listData = getListData(value);
  return (
    <ul className="events">
      {listData.map((item: any) => (
        <li key={item.content}>
          <Badge status={item.type} text={item.content} />
        </li>
      ))}
    </ul>
  );
}

function getMonthData(value: any) {
  if (value.month() === 8) {
    return "";
  }
}

function monthCellRender(value: any) {
  const num = getMonthData(value);
  return num ? (
    <div className="notes-month">
      <section>{num}</section>
      <span>Backlog number</span>
    </div>
  ) : null;
}

function TrainingSchedule() {
  return (
    <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
      <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
            onBack={() => window.history.back()}
            title="Training Schedule"
          >
          </PageHeader>
          <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
        </div>
        <h4 style={{ fontSize: 16, marginTop: 20, fontWeight: 500 }}>Calendar View</h4>
        <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
        <CardSchedule />
      </Card>
    </div>
  );
}

export default TrainingSchedule;
