import { Table, Button, Tag, Menu, Dropdown, } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const { Column, } = Table;
const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

const data = [
    {
        key: '1',
        Name: 'Sales',
        StartDate: '22 Juli 2021 08:00',
        EndDate: '24 Juli 2021 15:00',
        status: ['on-progress'],
    },
    {
        key: '2',
        Name: 'Project Management',
        StartDate: '2 Juli 2021 08:00',
        EndDate: '2 Juli 2021 15:00',
        status: ['completed'],
    },
    {
        key: '3',
        Name: 'Mentoring',
        StartDate: '22 Juli 2021 08:00',
        EndDate: '22 Juli 2021 15:00',
        status: ['completed'],
    },

];

function MyCompetencies() {

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <h4 style={{ marginBottom: 20 }}>My Competencies</h4>
            <Table dataSource={data} bordered={true} id="trheader">
                <Column title="Name" dataIndex="Name" key="Name" />
                <Column title="Start Date" dataIndex="StartDate" key="StartDate" />
                <Column title="End Date" dataIndex="EndDate" key="EndDate" />
                <Column
                    title="status"
                    dataIndex="status"
                    key="status"
                    render={status => (
                        <>
                            {status.map((tag: any) => (
                                tag === "on-progress" ?
                                    <Tag color="cyan" key={tag}>
                                        {tag}
                                    </Tag> :
                                    tag === "completed" ?
                                        <Tag color="blue" key={tag}>
                                            {tag}
                                        </Tag> :
                                        <Tag color="orange" key={tag}>
                                            {tag}
                                        </Tag>
                            ))}
                        </>
                    )}
                />
            </Table>
            {/* <Pagination defaultCurrent={6} total={500} /> */}
            <Dropdown overlay={menu} placement="bottomRight" arrow>
                <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
            </Dropdown>
        </div >
    )
}

export default MyCompetencies
