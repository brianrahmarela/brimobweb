import { Card, Table, PageHeader, Button, Divider, Tag, Menu, Dropdown, } from 'antd';
import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import { DownOutlined } from '@ant-design/icons';

import MyCompetencies from './myCompetencies';
import AvatarProfile from './avatarProfile';
const { Column, } = Table;
const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

// const suffix = (
//     <AudioOutlined
//         style={{
//             fontSize: 16,
//             color: '#1890ff',
//             border: 0,
//         }}
//     />
// );

const data = [
    {
        key: '1',
        TrainingName: 'Integritas',
        StartDate: '22 Juli 2021 08:00',
        EndDate: '22 Juli 2021 15:00',
        status: ['on-progress'],
    },
    {
        key: '2',
        TrainingName: 'Kerjasama',
        StartDate: '2 Juli 2021 08:00',
        EndDate: '2 Juli 2021 15:00',
        status: ['completed'],
    },
    {
        key: '3',
        TrainingName: 'Komunikasi',
        StartDate: '22 Juli 2021 08:00',
        EndDate: '22 Juli 2021 15:00',
        status: ['scheduled'],
    },
    {
        key: '4',
        TrainingName: 'Orientasi Pada Hasil',
        StartDate: '4 Juli 2021 08:00',
        EndDate: '4 Juli 2021 15:00',
        status: ['requested'],
    },
    {
        key: '5',
        TrainingName: 'Pelayanan Publik',
        StartDate: '22 Jan 2021 08:00',
        EndDate: '22 Jan 2021 15:00',
        status: ['cancelled'],
    },
    {
        key: '6',
        TrainingName: 'Mengelola Perubahan',
        StartDate: '14 Juli 2021 08:00',
        EndDate: '14 Juli 2021 15:00',
        status: ['assigned'],
    },
    {
        key: '7',
        TrainingName: 'Negosiasi',
        StartDate: '21 Juli 2021 08:00',
        EndDate: '21 Juli 2021 15:00',
        status: ['withdraw'],
    }, {
        key: '8',
        TrainingName: 'tujuh',
        StartDate: 'Brown',
        EndDate: 'End Date',
        status: ['withdraw'],
    }, {
        key: '9',
        TrainingName: 'delapan',
        StartDate: 'Brown',
        EndDate: 'End Date',
        status: ['on-progress'],
    }, {
        key: '10',
        TrainingName: 'sembilan',
        StartDate: 'Brown',
        EndDate: 'End Date',
        status: ['on-progress'],
    },
    {
        key: '11',
        TrainingName: 'sepuluh',
        StartDate: 'Green',
        EndDate: 'End Date',
        status: ['completed'],
    },
    {
        key: '12',
        TrainingName: 'sebelas',
        StartDate: 'Green',
        EndDate: 'End Date',
        status: ['completed'],
    },
];

function MyTrainingProfile() {
    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="My Training Profile"
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
                <AvatarProfile />
                <MyCompetencies />
                {/* <Search placeholder="Search for Competency" onSearch={onSearch} bordered={false} className="searchLearning" style={{ padding: 10, borderStyle: 'none' }} /> */}
                <Table dataSource={data} bordered={true} id="trheader">
                    <Column title="Training Name" dataIndex="TrainingName" key="TrainingName" />
                    <Column title="Start Date" dataIndex="StartDate" key="StartDate" />
                    <Column title="End Date" dataIndex="EndDate" key="EndDate" />
                    <Column
                        title="status"
                        dataIndex="status"
                        key="status"
                        render={status => (
                            <>
                                {status.map((tag: any) => (
                                    tag === "on-progress" ?
                                        <Tag color="cyan" key={tag}>
                                            {tag}
                                        </Tag> :
                                        tag === "completed" ?
                                            <Tag color="blue" key={tag}>
                                                {tag}
                                            </Tag> :
                                            tag === "scheduled" ?
                                                <Tag color="green" key={tag}>
                                                    {tag}
                                                </Tag> :
                                                tag === "requested" ?
                                                    <Tag color="pink" key={tag}>
                                                        {tag}
                                                    </Tag> :
                                                    tag === "cancelled" ?
                                                        <Tag color="default" key={tag}>
                                                            {tag}
                                                        </Tag> :
                                                        tag === "assigned" ?
                                                            <Tag color="red" key={tag}>
                                                                {tag}
                                                            </Tag> :

                                                            <Tag color="orange" key={tag}>
                                                                {tag}
                                                            </Tag>
                                ))}
                            </>
                        )}
                    />
                </Table>,
                {/* <Pagination defaultCurrent={6} total={500} /> */}
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
                </Dropdown>
            </Card>
        </div >
    )
}

export default MyTrainingProfile
