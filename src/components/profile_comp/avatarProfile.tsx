import React, { useState } from 'react';
import { Avatar, Image, Row, Col, Form, Input, Space } from 'antd';
import Avatar_MyProfile from '../../assets/images/img_avatar_myProfile.png'
function AvatarProfile() {
    const [size] = useState(42);

    const CustomizedForm = ({ onChange, fields }: { onChange: any, fields: any }) => (
        <Form
            name="global_state"
            fields={fields}
            onFieldsChange={(_, allFields) => {
                onChange(allFields);
            }}
            style={{padding:0, }}
            // id="formprofile"
        >
            <Form.Item
                style={{ height: 50, }}
                className="reducepaddinglabel"
                name="employee code"
                labelCol={{ span: 24 }}
                label="Employee code"
            >
                <Input />
            </Form.Item>
            <Form.Item
                style={{ height: 50 }}
                className="reducepaddinglabel"

                name="position"
                label="Position"
                labelCol={{ span: 24 }}
            >
                <Input />
            </Form.Item>
            <Form.Item
                style={{ height: 50 }}
                className="reducepaddinglabel"

                name="level"
                label="Level"
                labelCol={{ span: 24 }}
            >
                <Input />
            </Form.Item>
            <Form.Item
                style={{ height: 50 }}
                className="reducepaddinglabel"

                name="jobtitle"
                label="Job Title"
                labelCol={{ span: 24 }}
            >
                <Input />
            </Form.Item>
        </Form>
    );

    const [fields, setFields] = useState([
        {
            name: ['employee code'],
            value: 'A102305',
        },
        {
            name: ['position'],
            value: 'Department Head',
        },
        {
            name: ['level'],
            value: 'Senior Manager',
        },
        {
            name: ['jobtitle'],
            value: 'Head of Customer Service',
        },
    ]);
    return (
        <>
            <Row style={{ marginBottom: 58.5 }}>
                <Space size={size}>
                    <Col>
                        <Avatar shape="circle" size={253} icon={<Image src={Avatar_MyProfile} preview={false} />} />
                    </Col>
                    <Col >
                        <CustomizedForm
                            fields={fields}
                            onChange={(newFields: any) => {
                                setFields(newFields);
                            }}
                        />
                    </Col>
                </Space>
            </Row>
        </>
    );

}

export default AvatarProfile
