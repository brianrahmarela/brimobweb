import { Card, Row, Col, Image, Button, Divider, PageHeader, Input, Form, DatePicker, TimePicker, InputNumber, Typography } from 'antd';
import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import IconSubmitCheck from '../../assets/icons/icon_submit_check.svg';

//button icon handle
const onSearch = (value: any) => console.log(value);

const { RangePicker } = DatePicker;
const { Text } = Typography;

const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select time!' }],
};

function TrainingRequest() {
    const { Search } = Input;

    const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeValue = fieldsValue['range-training-picker'];
        const rangeTimeValue = fieldsValue['range-time-picker'];
        const values = {
            ...fieldsValue,
            'range-training-picker': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
            'range-time-picker': [
                rangeTimeValue[0].format('HH:mm:ss'),
                rangeTimeValue[1].format('HH:mm:ss'),
            ],
        };
        console.log('Received values of form: ', values);
    };

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Training Request"
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
                <Form name="time_related_controls" onFinish={onFinish}>
                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="Training Name"
                        name="trainingname"
                    >
                        <Row>
                            <Col span={20}>
                                <Input />
                            </Col>
                            <Col span={4}>
                                <Search placeholder="Search" onSearch={onSearch} bordered={false} className="searchTraining" style={{ borderStyle: 'none' }} />
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}

                        label="Training Description"
                        name="trainingdescription"
                    >
                        <Input.TextArea style={{ height: 100 }} />
                    </Form.Item>
                    <Form.Item labelCol={{ span: 24 }} name="range-training-picker" label="Training Schedule" {...rangeConfig}>
                        <RangePicker />
                    </Form.Item>
                    <Form.Item labelCol={{ span: 24 }}
                        name="range-time-picker" label="Time" {...rangeConfig}>
                        <TimePicker.RangePicker use12Hours format="HH:mm:ss" />
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}

                        label="Place"
                        name="place"
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}

                        label="Estimated Cost"
                        name="estimatedcost"
                    >
                        <InputNumber />
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}

                        label="Outcome"
                        name="outcome"
                    >
                        <Input.TextArea style={{ height: 100 }} />
                    </Form.Item>
                    <Form.Item
                        labelCol={{ span: 24 }}

                    // wrapperCol={{
                    //     xs: { span: 24, offset: 0 },
                    //     sm: { span: 16, offset: 8 },
                    // }}
                    >
                        <Button type="primary" htmlType="submit" style={{ padding: '7px 22px 7px 5.6px', height: 44, borderRadius: 10, backgroundColor: 'white', borderStyle: 'none' }} className="searchLearning">
                            <Row align="middle" justify="start">
                                <Image src={IconSubmitCheck} preview={false} height={30} style={{ marginRight: 10.4 }} />
                                <Text style={{ color: '#086E3A', fontSize: 16, }}>Submit</Text>
                            </Row>
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div >
    )
}

export default TrainingRequest
