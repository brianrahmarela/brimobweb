import React, { useState, useEffect } from 'react'
import { Card, Row, Col, Image, Space } from "antd";
import { DualAxes } from '@antv/g2plot';
import IconTotalPersonil from '../../assets/icons/icon_total_Personil.svg';
import IconJmlGap from '../../assets/icons/icon_jml_Gap.svg';
import IconTotalPendidikan from '../../assets/icons/icon_total_Pendidikan.svg';
import IconProgressPenyelesaian from '../../assets/icons/icon_progress_Penyelesaian.svg';
import lineTotalPersonil from '../../assets/icons/line_total_Personil.svg';
import lineJmlGap from '../../assets/icons/line_jml_Gap.svg';
import lineTotalPendidikan from '../../assets/icons/line_total_Pendidikan.svg';
import lineProgressPenyelesaian from '../../assets/icons/line_progress_Penyelesaian.svg';

//chart komponen
import GapKompetensi from '../dashboard_comp/GapKompetensi'
import TrainingKompetensi from '../dashboard_comp/TrainingKompetensi'

function PencapaianKompetensi() {
    const [uvBillData] = useState([
        { month: 'JAN', value: 20, type: 'Belum' },
        { month: 'FEB', value: 100, type: 'Belum' },
        { month: 'MAR', value: 60, type: 'Belum' },
        { month: 'APR', value: 30, type: 'Belum' },
        { month: 'MAY', value: 110, type: 'Belum' },
        { month: 'JUN', value: 140, type: 'Belum' },
        { month: 'JAN', value: 90, type: 'Selesai' },
        { month: 'FEB', value: 80, type: 'Selesai' },
        { month: 'MAR', value: 70, type: 'Selesai' },
        { month: 'APR', value: 85, type: 'Selesai' },
        { month: 'MAY', value: 25, type: 'Selesai' },
        { month: 'JUN', value: 80, type: 'Selesai' },
        { month: 'JAN', value: 115, type: 'Sedang' },
        { month: 'FEB', value: 65, type: 'Sedang' },
        { month: 'MAR', value: 60, type: 'Sedang' },
        { month: 'APR', value: 50, type: 'Sedang' },
        { month: 'MAY', value: 48, type: 'Sedang' },
        { month: 'JUN', value: 20, type: 'Sedang' },

    ] as any[]);

    const [transformData] = useState([
    ]);

    useEffect(() => {
        const container: any = document.getElementById("container-cart2");
        if (container != null) {
            const dualAxes = new DualAxes(container, {
                data: [uvBillData, transformData],
                xField: 'month',
                yField: ['value', 'count'],
                height: 300,
                geometryOptions: [
                    {
                        geometry: 'line',
                        seriesField: 'type',
                        lineStyle: {
                            lineWidth: 3,
                        },
                        smooth: false,
                        point: {
                            shape: 'circle',
                            size: 4,
                            style: {
                                opacity: 0.5,
                                stroke: '#5AD8A6',
                                fill: '#fff',
                            },
                        },
                    },
                    {
                        geometry: 'line',
                        seriesField: 'name',
                        point: {},
                    },
                ],
            });
            dualAxes.update({ "theme": { "styleSheet": { "brandColor": "#FF4500", "paletteQualitative10": ["#EC6666", "#147AD6", "#79D2DE",] } } });
            dualAxes.render();
        }
    }, [uvBillData, transformData]);

    return (
        <div className="site-card-wrapper" style={{ margin: "30px 0px" }}>
            <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
                <Row
                    justify="space-between"
                    style={{ marginBottom: 27, fontWeight: 600, fontSize: 18 }}
                >
                    Pencapaian Kompetensi
                </Row>

                <Row gutter={[56, 16]}>
                    <Col span={8} xs={24} md={24} lg={8} style={{marginBottom: 40}}>
                        <TrainingKompetensi />
                    </Col>
                    <Col span={8} xs={24} md={24} lg={8} style={{marginBottom: 40}}>
                        <Row justify="center">
                            <h1 style={{ fontSize: 16 }}>Jumlah Training</h1>
                        </Row>
                        <Row justify="center">
                            <p style={{ fontSize: 14, color: '#7C828A', fontWeight: 400 }}>01 Jan - 21 April</p>
                        </Row>
                        <div id="container-cart2"></div>

                    </Col>
                    <Col span={8} xs={24} md={24} lg={8} >
                        <GapKompetensi />
                    </Col>
                </Row>

                <Row >
                    <Col span={6} xs={24} md={24} lg={6}>
                        <Card className="radiusCardLeft" style={{ padding: 21 }}>
                            <Row justify="space-between" align="middle">
                                <Col>
                                    <Row justify="start">
                                        <Space size={8} >
                                            <Col>
                                                <Image src={IconTotalPersonil} preview={false} />
                                            </Col>

                                            <Col >Total Personil</Col>
                                        </Space>
                                    </Row>
                                    <Row justify="start">
                                        <Col style={{ fontFamily: 'Poppins', fontSize: 40, fontWeight: 700 }}>
                                            14.231
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Image src={lineTotalPersonil} preview={false} />
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                    <Col span={6} xs={24} md={12} lg={6}>
                        <Card style={{ padding: 21 }}>
                            <Row justify="space-between" align="middle">
                                <Col>
                                    <Row justify="start">
                                        <Space size={8} >
                                            <Col>
                                                <Image src={IconJmlGap} preview={false} />
                                            </Col>
                                            <Col style={{ color: "#707070" }}>
                                                Jumlah Gap Kompetensi
                                            </Col>
                                        </Space>
                                    </Row>
                                    <Row justify="start">
                                        <Col style={{ fontFamily: 'Poppins', fontSize: 40, fontWeight: 700 }}>
                                            48.231
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Image src={lineJmlGap} preview={false} />
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                    <Col span={6} xs={24} md={12} lg={6}>
                        <Card style={{ padding: 21 }}>
                            <Row justify="space-between" align="middle">
                                <Col>
                                    <Row justify="start">
                                        <Space size={8} >
                                            <Col>
                                                <Image src={IconTotalPendidikan} preview={false} />
                                            </Col>
                                            <Col style={{ color: "#707070" }}>Total Pendikan & Pelatihan</Col>
                                        </Space>
                                    </Row>
                                    <Row justify="start">
                                        <Col style={{ fontFamily: 'Poppins', fontSize: 40, fontWeight: 700 }}>
                                            451
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Image src={lineTotalPendidikan} preview={false} />
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                    <Col span={6} xs={24} md={12} lg={6}>
                        <Card className="radiusCardRight" style={{ padding: 21 }}>
                            <Row justify="space-between" align="middle">
                                <Col>
                                    <Row justify="start">
                                        <Space size={8} >
                                            <Col>
                                                <Image src={IconProgressPenyelesaian} preview={false} height={20} />
                                            </Col>
                                            <Col style={{ color: "#707070" }}>Progress Penyelesaian Gap</Col>
                                        </Space>
                                    </Row>
                                    <Row justify="start">
                                        <Col style={{ fontFamily: 'Poppins', fontSize: 40, fontWeight: 700 }}>
                                            38%
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Image src={lineProgressPenyelesaian} preview={false} />
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default PencapaianKompetensi
