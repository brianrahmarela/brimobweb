import { Row, Col, Card, Menu, Dropdown, Button, } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const menu = (
  <Menu>
    <Menu.Item key={1}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Personel 1
      </a>
    </Menu.Item>
    <Menu.Item key={2}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Personel 2
      </a>
    </Menu.Item>
    <Menu.Item key={3}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        Personel 3
      </a>
    </Menu.Item>
    <Menu.Item key={4}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        Personel 4
      </a>
    </Menu.Item>
    <Menu.Item key={5}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        Personel 5
      </a>
    </Menu.Item>
  </Menu>
);

function PetaAlokasiPersonil() {
  return (
    <div className="site-card-wrapper" style={{ margin: "30px 0px" }}>
      <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
        <Row justify="space-between" style={{ marginBottom: 27 }}>
          <Col style={{ fontWeight: 600, fontSize: 18 }}>
            Peta Alokasi Personil
          </Col>
          <Col >
            <Row justify="end">
              <Dropdown overlay={menu} >
                <Button style={{ fontSize: 13, fontWeight: 400 }}>
                  Personel Bantuan <DownOutlined style={{ color: 'black' }} />
                </Button>
              </Dropdown>
            </Row>
          </Col>
        </Row>
        <div className="map-responsive">
          <iframe src="https://www.google.com/maps/d/embed?mid=1oeDeRj0jPjuJGiIpOLYLeXPJIFuLb2uK" width="640" height="480" title="Peta Alokasi Personil"></iframe>
        </div>
      </Card>
    </div>
  )
}

export default PetaAlokasiPersonil
