import React, { useState } from 'react'
import { Row, Col, Card, Image, Divider, Menu, Dropdown, Button, Space, Typography } from 'antd';
import imgLaporan from '../../assets/images/img_laporan_lap.png'
import IconWeb from '../../assets/icons/icon_web.svg';
import IconPhone from '../../assets/icons/icon_phone.svg';
import IconArrowDiagonal from '../../assets/icons/icon_arrow_diagonal.svg';
import { DownOutlined } from '@ant-design/icons';

const menu = (
    <Menu>
        <Menu.Item key={1}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                Update 1
            </a>
        </Menu.Item>
        <Menu.Item key={2}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                Update 2
            </a>
        </Menu.Item>
        <Menu.Item key={3}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Update 3
            </a>
        </Menu.Item>
        <Menu.Item key={4}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Update 4
            </a>
        </Menu.Item>
        <Menu.Item key={5}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Update 5
            </a>
        </Menu.Item>
    </Menu>
);

function LiveUpdate() {

    const [size] = useState(88);
    const { Text } = Typography;

    return (
        <div className="site-card-wrapper" style={{ margin: '30px 0px' }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>

                <Row justify="space-between" style={{ marginBottom: 27,  }}>
                    <Col style={{ fontWeight: 600, fontSize: 18, marginBottom: 10 }}>
                        LIVE UPDATE LAPORAN LAPANGAN
                    </Col>
                    <Col >
                        <Row justify="end">
                            <Dropdown overlay={menu}>
                                <Button style={{ fontSize: 13, fontWeight: 400,}}>
                                    Semua Update <DownOutlined style={{ color: 'black' }} />
                                </Button>
                            </Dropdown>
                        </Row>
                    </Col>
                </Row>
                <Space size={67} direction="vertical">

                    <Row >
                        <Col lg={10} xs={24}>
                            <Image src={imgLaporan} preview={false} style={{marginBottom: 10}}/>
                        </Col>
                        <Col lg={1} xs={0}>
                        </Col>
                        <Col lg={13} xs={24}>
                            <Row style={{ fontWeight: 600, fontSize: 22, marginBottom: 9.8 }}>
                                PPKM Jabar
                            </Row>
                            <Row style={{ color: '#707070' }}>
                                <Col span={19}>

                                    Anggota Kompi 1 Batalyon C Pelopor Satbrimob Polda Jabar Dipimpin Aipda Beni Setiawan menjalankan program pemberlakuan pembatasan kegiatan masyarakat ( PPKM ) bersama anggota TNI dan Polres Cirebon Kota, hal tersebut
                                </Col>
                            </Row>
                            <Divider />
                            <Row justify="space-between">
                                <Row justify="space-between">
                                    <Space size={size} >
                                        <Col style={{ marginBottom: 8 }}><Image src={IconWeb} preview={false} height={20} /></Col>
                                        <Col style={{ marginBottom: 8 }}><Image src={IconPhone} preview={false} height={20} /></Col>
                                    </Space>
                                </Row>
                                <Row justify="end" >
                                    <Col style={{ color: '#707070' }} >Cirebon, 21 Juli 2021 19:00</Col>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                    <Row justify="end">
                        <Space size={9} >
                            <Row justify="end">
                                <Col><a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF', fontSize: 13 }}>Lihat semuanya</Text></a>
                                </Col>
                            </Row>
                            <Row justify="end">
                                <Col><Image src={IconArrowDiagonal} preview={false} height={11} /></Col>
                            </Row>
                        </Space>
                    </Row>
                </Space>
            </Card>
        </div>
    )
}

export default LiveUpdate
