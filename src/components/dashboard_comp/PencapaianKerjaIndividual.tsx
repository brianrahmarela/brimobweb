import { Row, Col, Card, Progress, Space, Avatar } from 'antd';
import { Chart, registerShape } from '@antv/g2';

import React, { useState, useEffect } from 'react'

function PencapaianKerjaIndividual() {
    const [data] = useState([{ value: 6 }]);

    useEffect(() => {
        registerShape('point', 'pointer', {
            // draw(cfg, container) {
            //     const group = container.addGroup();
            //     return group;
            // },
            draw(cfg, container) {
                const group = container.addGroup();
                // const center = this.parsePoint({ x: 0, y: 0 }); // 获取极坐标系下画布中心点
                // 绘制指针
             
            
                return group;
              },
        });

        const container: any = document.getElementById("container-cart5");
        if (container != null) {
            const chart = new Chart({
                container,
                autoFit: true,
                height: 250,
                padding: [0, 0, 30, 0],

            });

            chart.data(data);
            chart.scale('value', {
                min: 0,
                max: 10,
                tickInterval: 1,
            });
            chart.coordinate('polar', {
                startAngle: (10 / 10) * Math.PI,
                endAngle: (0 / 10) * Math.PI,
                radius: 0.75,
            });

            chart.axis('1', false);
            chart.axis('value', {
                line: null,
                label: {
                    offset: -36,
                    style: {
                        fontSize: 18,
                        textAlign: 'center',
                        textBaseline: 'middle',
                    },
                },
                subTickLine: {
                    count: 1,
                    length: -15,
                },
                tickLine: {
                    length: 0,
                },
                grid: null,
            });
            chart.legend(false);
            chart
                .point()
                .position('value*1')
                .shape('pointer')
                .color('#00EDBF')
                .animate({
                    appear: {
                        animation: 'fade-in'
                    }
                });
            chart.annotation().arc({
                top: false,
                start: [0, 1],
                end: [10, 1],
                style: {
                    // 底灰色
                    stroke: '#FF8373',
                    lineWidth: 45,
                    lineDash: null,
                },
            });
            chart.annotation().arc({
                start: [0, 1],
                end: [data[0].value, 1],
                style: {
                    stroke: '#00EDBF',
                    lineWidth: 45,
                    lineDash: null,
                },
            });
            chart.annotation().text({
                position: ['50%', '90%'],
                content: `${data[0].value * 10}%`,
                style: {
                    fontFamily: 'Poppins',
                    fontSize: 35,
                    fill: '#545454',
                    textAlign: 'center',
                    fontWeight: 600
                },
                offsetY: 15,
            });

            chart.render();
        }
    }, [data]);

    return (
        <div className="site-card-wrapper" style={{ margin: "30px 0px" }}>
            <Row gutter={16}>
                <Col span={12} xs={24} md={24} lg={12}>
                    <Card bordered={false} style={{ borderRadius: '3px', padding: "20px" }}>
                        <Row style={{ marginBottom: 27, fontWeight: 600, fontSize: 18 }}>
                            Pencapaian Kinerja Individual
                        </Row>
                        <Row justify="start">
                            <Col style={{ fontSize: 18, fontWeight: 400 }}>Rata-Rata</Col>
                        </Row>
                        <Row justify="start">
                            <Col style={{ fontSize: 32, fontWeight: 600 }}>65%</Col>
                        </Row>
                        <Row>
                            <Progress percent={65} showInfo={false} strokeWidth={25} strokeColor="#E4E9F2" trailColor="#F7F9FC" strokeLinecap="square" />
                        </Row>
                        <Space size={38} direction="vertical">
                            <p style={{ color: "#8D98AB", marginTop: 8 }}>Lebih baik dari bulan lalu (60%)</p>
                            <Row justify="start">
                                <Col style={{ fontSize: 18, fontWeight: 400 }}>Tertinggi</Col>
                            </Row>
                        </Space>
                        <Row justify="start">
                            <Col style={{ fontSize: 32, fontWeight: 600 }}>81%</Col>
                        </Row>
                        <Row>
                            <Progress percent={81} showInfo={false} strokeWidth={25} strokeColor="#E4E9F2" trailColor="#F7F9FC" strokeLinecap="square" />
                        </Row>
                        <Space size={38} direction="vertical">
                            <p style={{ color: "#8D98AB", marginTop: 8 }}>Lebih baik dari bulan lalu (79%)</p>
                            <Row justify="start">
                                <Col style={{ fontSize: 18, fontWeight: 400 }}>Terendah</Col>
                            </Row>
                        </Space>
                        <Row justify="start">
                            <Col style={{ fontSize: 32, fontWeight: 600 }}>40%</Col>
                        </Row>
                        <Row>
                            <Progress percent={65} showInfo={false} strokeWidth={25} strokeColor="#E4E9F2" trailColor="#F7F9FC" strokeLinecap="square" />
                        </Row>
                        <p style={{ color: "#8D98AB", marginTop: 8 }}>Lebih buruk dari bulan lalu (45%)</p>

                    </Card>
                </Col>
                <Col span={12} xs={24} md={24} lg={12}>
                    <Card bordered={false} style={{ borderRadius: '3px', padding: "20px" }}>
                        <Row style={{ marginBottom: 27, fontWeight: 600, fontSize: 18 }}>
                            Pencapaian Kinerja Kesatuan
                        </Row>
                        <div id="container-cart5"></div>
                        <Row justify="space-around" style={{ marginTop: 15 }}>
                            <Col >
                                <Row >
                                    <Row align="top" style={{ marginRight: 8 }}>

                                        <Col >
                                            <Avatar size={10} style={{ backgroundColor: '#00EDBF' }} />
                                        </Col>
                                    </Row>
                                    <Row align="top">

                                        <Col>
                                            <Row >
                                                <Col style={{ fontSize: 16, fontWeight: 500 }}>Sesuai Standard</Col>
                                            </Row>
                                            <Col style={{ color: "#707070", fontFamily: 'Poppins', fontSize: 16, fontWeight: 400 }}>
                                                116 Penugasan
                                            </Col>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                            <Col >
                                <Row >
                                    <Row align="top" style={{ marginRight: 8 }}>
                                        <Col >
                                            <Avatar size={10} style={{ backgroundColor: "#FF8373" }} />
                                        </Col>
                                    </Row>
                                    <Row align="top">

                                        <Col>
                                            <Row >
                                                <Col style={{ fontSize: 16, fontWeight: 500 }}>Tidak Sesuai Standard</Col>
                                            </Row>
                                            <Col style={{ color: "#707070", fontFamily: 'Poppins', fontSize: 16, fontWeight: 400 }}>
                                                18 Penugasan
                                            </Col>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: 58, marginBottom: 27, fontWeight: 600, fontSize: 18 }}>
                            Daerah dengan Pencapaian Kinerja Terburuk
                        </Row>
                        <Row style={{ marginBottom: 12 }}>
                            <Col span={6} xs={24} md={12} lg={6}>Papua</Col>
                            <Col span={18}>

                                <Progress percent={40} strokeColor="#6C5DD3" trailColor="#E1E1E1" style={{ fontWeight: 600, fontSize: 16 }} />
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 12 }}>
                            <Col span={6} xs={24} md={12} lg={6}>Makassar</Col>
                            <Col span={18}>
                                <Progress percent={60} strokeColor="#6C5DD3" trailColor="#E1E1E1" style={{ fontWeight: 600, fontSize: 16 }} />
                            </Col>
                        </Row>
                        <Row>
                            <Col span={6} xs={24} md={12} lg={6}>Lampung</Col>
                            <Col span={18}>
                                <Progress percent={75} strokeColor="#6C5DD3" trailColor="#E1E1E1" style={{ fontWeight: 600, fontSize: 16 }} />
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default PencapaianKerjaIndividual
