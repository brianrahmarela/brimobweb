import { Pie } from '@antv/g2plot';
import { Row, } from "antd";

import React, { useState, useEffect } from 'react'

function GapKompetensi() {
  const [data] = useState([
    { type: 'Selesai', value: 42 },
    { type: 'Sedang', value: 40 },
    { type: 'Belum', value: 18 },
  ]);

  useEffect(() => {
    const container: any = document.getElementById("container-cart3");
    if (container != null) {
      const piePlot = new Pie(container, {
        appendPadding: 10,
        data,
        angleField: 'value',
        colorField: 'type',
        radius: 0.9,
        label: {
          type: 'inner',
          offset: '-30%',
          content: ({ percent }) => `${(percent * 100).toFixed(0)}%`,
          style: {
            fontSize: 14,
            textAlign: 'center',
          },
        },
        interactions: [{ type: 'element-active' }],
      });
      piePlot.update({ "theme": { "styleSheet": { "brandColor": "#FF4500", "paletteQualitative10": ["#147AD6", "#79D2DE", "#EC6666",], } } });

      piePlot.render();
    }
  }, [data]);
  return (
    <div>
      <Row justify="center">
        <h1 style={{ fontSize: 16 }}>Gap Kompetensi</h1>
      </Row>
      <Row justify="center">
        <p style={{ fontSize: 14, color: '#7C828A', fontWeight: 400 }}>01 Jan - 31 Dec</p>
      </Row>
      <div id="container-cart3"></div>
    </div>
  )
}

export default GapKompetensi
