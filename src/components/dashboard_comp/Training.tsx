import { Column } from "@antv/g2plot";
import { Card, Row, Tabs, Col, Menu, Dropdown, Button } from "antd";
import { useEffect } from "react";
import { useState } from "react";
import { DownOutlined, } from '@ant-design/icons';

const menu = (
    <Menu>
        <Menu.Item key={1}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                Jan
            </a>
        </Menu.Item>
        <Menu.Item key={2}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                Feb
            </a>
        </Menu.Item>
        <Menu.Item key={3}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Mar
            </a>
        </Menu.Item>
        <Menu.Item key={4}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Apr
            </a>
        </Menu.Item>
        <Menu.Item key={5}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Mei
            </a>
        </Menu.Item>
        <Menu.Item key={6}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Jun
            </a>
        </Menu.Item>
        <Menu.Item key={7}>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Jul
            </a>
        </Menu.Item>
    </Menu>
);

function Training() {
    const { TabPane } = Tabs;
    const [data] = useState([
        {
            name: 'Selesai',
            month: 'Jan',
            jumlah: 480,
        },
        {
            name: 'Selesai',
            month: 'Feb',
            jumlah: 225,
        },
        {
            name: 'Selesai',
            month: 'Mar',
            jumlah: 370,
        },
        {
            name: 'Selesai',
            month: 'Apr',
            jumlah: 230,
        },
        {
            name: 'Selesai',
            month: 'Mei',
            jumlah: 200,
        },
        {
            name: 'Selesai',
            month: 'Jun',
            jumlah: 80,
        },
        {
            name: 'Selesai',
            month: 'Jul',
            jumlah: 310,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Jan',
            jumlah: 220,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Feb',
            jumlah: 230,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Mar',
            jumlah: 150,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Apr',
            jumlah: 160,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Mei',
            jumlah: 470,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Jun',
            jumlah: 60,
        },
        {
            name: 'Sedang Berlangsung',
            month: 'Jul',
            jumlah: 300,
        },
        {
            name: 'Di tugaskan',
            month: 'Jan',
            jumlah: 130,
        },
        {
            name: 'Di tugaskan',
            month: 'Feb',
            jumlah: 70,
        },
        {
            name: 'Di tugaskan',
            month: 'Mar',
            jumlah: 170,
        },
        {
            name: 'Di tugaskan',
            month: 'Apr',
            jumlah: 310,
        },
        {
            name: 'Di tugaskan',
            month: 'Mei',
            jumlah: 370,
        },
        {
            name: 'Di tugaskan',
            month: 'Jun',
            jumlah: 30,
        },
        {
            name: 'Di tugaskan',
            month: 'Jul',
            jumlah: 490,
        },

    ] as any[]);

    useEffect(() => {
        const container: any = document.getElementById("container-cart");
        if (container != null) {
            const stackedColumnPlot = new Column(container, {
                data,
                isGroup: true,
                xField: 'month',
                yField: 'jumlah',
                seriesField: 'name',
                padding: 70,
                color: ['#EDF1F7', '#73A1FF', '#5CCFE2'],
                marginRatio: 0,
                theme: {
                    styleSheet: {
                      fontFamily: 'Poppins',
                      
                    }
                  }
            });
            stackedColumnPlot.render();
        }
    }, [data]);

    return (
        <div className="site-card-wrapper" style={{ margin: "30px 0px" }}>
            <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
                <Row
                    justify="space-between"
                    style={{ marginBottom: 27, fontWeight: 600, fontSize: 18 }}
                >
                    Training
                </Row>
                <Tabs defaultActiveKey="trainingteknis" centered tabBarGutter={100} >
                    <TabPane tab="Training Teknis" key="trainingteknis">
                        <Card style={{ backgroundColor: "#EDF1F7" }}>
                            <Row >
                                <Col span={8}>
                                    <Card bordered={false} style={{ backgroundColor: "#EDF1F7" }}>
                                        <Row align="middle">
                                            <Col>
                                                <Row justify="start">
                                                    <Col >Ditugaskan</Col>
                                                </Row>
                                                <Row justify="start">
                                                    <Col style={{ fontSize: 19, fontWeight: 600 }}>
                                                        3654
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col span={8}>
                                    <Card bordered={false} style={{ backgroundColor: "#EDF1F7" }}>
                                        <Row align="middle">
                                            <Col>
                                                <Row justify="start">
                                                    <Col style={{ color: "#707070" }}>
                                                        Sedang Berlangsung
                                                    </Col>
                                                </Row>
                                                <Row justify="start">
                                                    <Col style={{ fontSize: 19, fontWeight: 600 }}>
                                                        946
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col span={8}>
                                    <Card bordered={false} style={{ backgroundColor: "#EDF1F7" }}>
                                        <Row align="middle">
                                            <Col>
                                                <Row justify="start">
                                                    <Col style={{ color: "#707070" }}>Sudah Selesai</Col>
                                                </Row>
                                                <Row justify="start">
                                                    <Col style={{ fontSize: 19, fontWeight: 600 }}>
                                                        654
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                            </Row>
                        </Card>
                        <Row justify="end" style={{marginBottom: 40}}>

                            <Dropdown overlay={menu} >
                                <Button style={{top: 20, position: 'relative'}}>
                                    Month <DownOutlined />
                                </Button>
                            </Dropdown>
                        </Row>

                        <div id="container-cart"></div>
                    </TabPane>
                    <TabPane tab="Training Manajerial" key="trainingmanajerial">

                    </TabPane>
                </Tabs>
            </Card>
        </div>
    );
}

export default Training;
