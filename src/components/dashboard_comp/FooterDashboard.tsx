import React, { useState } from 'react'
import { Card, Col, Row, Image, Space, } from 'antd';
import LogoLinkedIn from '../../assets/icons/logo_linkedin.svg';
import LogoTwitter from '../../assets/icons/logo_twitter.svg';
import LogoFacebook from '../../assets/icons/logo_facebook.svg';
import LogoTrello from '../../assets/icons/logo_trello.svg';

function FooterDashboard() {
    const [size] = useState(19.1);

    return (
        <div className="site-card-wrapper" >
            <Card bordered={false} style={{ borderRadius: "3px", padding: "10px" }}>
                <Row justify="space-between" align="middle" style={{ height: 96,  }}>
                    <Col style={{ fontSize: 19, marginBottom: 20 }} md={12} lg={12}>TALENTHRM @2021. https://talenthrm.com</Col>
                    <Col md={12} lg={12}>
                    
                    <Row justify="end" style={{ marginRight: 20, marginBottom: 20}}>
                        <Space size={size}>
                            <a href="https://id.linkedin.com/company/pt-talenta-sumberdaya-manusia"><Image src={LogoTrello} preview={false} /></a>
                            <a href="https://id.linkedin.com/company/pt-talenta-sumberdaya-manusia"> <Image src={LogoFacebook} preview={false} /></a>
                            <a href="https://id.linkedin.com/company/pt-talenta-sumberdaya-manusia"> <Image src={LogoTwitter} preview={false} /></a>
                            <a href="https://id.linkedin.com/company/pt-talenta-sumberdaya-manusia"><Image src={LogoLinkedIn} preview={false} /></a>
                        </Space>
                    </Row>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default FooterDashboard
