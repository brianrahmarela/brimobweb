import { DualAxes } from '@antv/g2plot';
import React, { useState, useEffect } from 'react'
import { Row } from "antd";

function TrainingKompetensi() {
    const [data] = useState([
        { year: 'JAN', selesai: 500, belum: 260 },
        { year: 'FEB', selesai: 380, belum: 490 },
        { year: 'MAR', selesai: 720, belum: 150 },
        { year: 'APR', selesai: 510, belum: 250 },
        { year: 'MAY', selesai: 100, belum: 260 },
        { year: 'JUN', selesai: 260, belum: 170 },
    ]);

    useEffect(() => {
        const container: any = document.getElementById("container-cart1");
        if (container != null) {
            const dualAxes = new DualAxes(container, {
                data: [data, data],
                xField: 'year',
                yField: ['selesai', 'belum'],
                height: 300,
                geometryOptions: [
                    {
                        geometry: 'line',
                        smooth: true,
                        color: '#147AD6',
                        lineStyle: {
                            lineWidth: 3,
                        },
                    },
                    {
                        geometry: 'line',
                        smooth: true,
                        color: '#EC6666',
                        lineStyle: {
                            lineWidth: 3,
                        },
                    },
                ],
            });

            dualAxes.render();
        }
    }, [data]);

    return (
        <div>
            <Row justify="center">
                <h1 style={{ fontSize: 16 }}>Training Kompetensi</h1>
            </Row>
            <Row justify="center">
                <p style={{ fontSize: 14, color: '#7C828A', fontWeight: 400 }}>01 Jan - 21 April</p>
            </Row>
            <div id="container-cart1"></div>
        </div>
    )
}

export default TrainingKompetensi
