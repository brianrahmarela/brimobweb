import { Card, Row, Table, Space, Col, Typography, Image, } from 'antd';
import { CaretUpOutlined, CaretDownOutlined, } from '@ant-design/icons';
import IconArrowDiagonal from '../../assets/icons/icon_arrow_diagonal.svg';

function DaerahPenugasan() {
    const { Column } = Table;
    const { Text } = Typography;

    const data = [
        {
            key: '1',
            daerah: 'Aceh',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 661,
            penambahanpersonil: "35%",
        },
        {
            key: '2',
            daerah: 'Sulawesi Tengah',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 359,
            penambahanpersonil: "4%",
        },
        {
            key: '3',
            daerah: 'Pekanbaru',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 671,
            penambahanpersonil: "23%",
        },
        {
            key: '4',
            daerah: 'Lampung',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 274,
            penambahanpersonil: "34%",
        },
        {
            key: '5',
            daerah: 'DKI Jakarta',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 398,
            penambahanpersonil: "42%",
        },
        {
            key: '6',
            daerah: 'Papua',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 306,
            penambahanpersonil: "15%",
        },
        {
            key: '7',
            daerah: 'Makassar',
            week: "Detail",
            sorter: {
                compare: (a: any, b: any) => a.week - b.week,
                multiple: 3,
            },
            totalpersonil: 398,
            penambahanpersonil: "42%",
        },
    ];

    return (
        <div className="site-card-wrapper" style={{ margin: '30px 0px' }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <Row justify="space-between" style={{ marginBottom: 27, fontWeight: 600, fontSize: 18 }}>

                    Daerah Penugasan
                </Row>
                <Table
                    dataSource={data}
                >
                    <Column
                        title="Daerah"
                        dataIndex="daerah"
                        key="daerah"

                        render={(daerah) => (
                            <>
                                {daerah}
                            </>
                        )}
                    />
                    <Column
                        title="Total Personil"
                        dataIndex="totalpersonil"
                        key="totalpersonil"

                        render={(totalpersonil) => (
                            <>
                                {totalpersonil}
                            </>
                        )}
                    />
                    <Column
                        title="Penambahan Personil"
                        dataIndex="penambahanpersonil"
                        key="penambahanpersonil"                    
                        render={(penambahanpersonil) => (
                            <>
                                {
                                    penambahanpersonil === "23%" ? (
                                        <div style={{ color: "#00D68F" }}><CaretUpOutlined /><Text style={{ color: "#00D68F" }}>{penambahanpersonil}</Text></div>
                                    ) : penambahanpersonil <= "35%" ? (
                                        <div style={{ color: "#FF3D71" }}><CaretDownOutlined /> <Text style={{ color: "#FF3D71" }}>{penambahanpersonil}</Text></div>

                                    ) : (
                                        <div style={{ color: "#00D68F" }}><CaretUpOutlined /><Text style={{ color: "#00D68F" }}>{penambahanpersonil}</Text></div>
                                    )
                                }
                            </>
                        )}
                    />
                    <Column
                        title="Week"
                        dataIndex="week"
                        key="week"
                        sorter={true}
                        render={(week) => (
                            <>
                                {<a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF' }}>{week}</Text></a>}
                            </>
                        )}
                    />
                </Table>
                <Row justify="end">
                    <Space size={9} >
                        <Row justify="end">
                            <Col><a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF', fontSize: 13 }}>Lihat semuanya</Text></a>
                            </Col>
                        </Row>
                        <Row justify="end">
                            <Col><Image src={IconArrowDiagonal} preview={false} height={11} /></Col>
                        </Row>
                    </Space>
                </Row>
            </Card>
        </div>
    )
}

export default DaerahPenugasan
