import { Card, Table, PageHeader, Button, Divider, Input, Menu, Dropdown, Typography, } from 'antd';

import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import IconAdd from '../../assets/icons/icon_add.svg';
import IconDownload from '../../assets/icons/icon_download.svg';
import IconPrint from '../../assets/icons/icon_print.svg';
import { DownOutlined } from '@ant-design/icons';

const { Column, } = Table;
const menuKamus = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
            Kamus Kompetensi 2021
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
            Kamus Kompetensi 2020
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
            Kamus Kompetensi 2019
            </a>
        </Menu.Item>
    </Menu>
);

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

const data = [
    {
        key: '1',
        name: 'Integritas',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '2',
        name: 'Kerjasama',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '3',
        name: 'Komunikasi',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '4',
        name: 'Orientasi Pada Hasil',
        type: 'Manajerial',

        appliedtojobs: '85 position',
    },
    {
        key: '5',
        name: 'Pelayanan Publik',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '6',
        name: 'Mengelola Perubahan',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '7',
        name: 'Negosiasi',
        type: 'Teknis',
        appliedtojobs: '24 position',
    }, {
        key: '8',
        name: 'tujuh',
        type: 'Teknis',
        appliedtojobs: '24 position',
    }, {
        key: '9',
        name: 'delapan',
        type: 'Teknis',
        appliedtojobs: '24 position',
    }, {
        key: '10',
        name: 'sembilan',
        type: 'Manajerial',
        appliedtojobs: '24 position',
    },
    {
        key: '11',
        name: 'sepuluh',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
    {
        key: '12',
        name: 'sebelas',
        type: 'Manajerial',
        appliedtojobs: '85 position',
    },
];

//button icon handle
const onSearch = (value: any) => console.log(value);

const handleAdd = (value: any) => {
    console.log("add my training");
}
const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}


function MyTrainingComp() {
    const { Search } = Input;
    const { Text } = Typography;

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
            <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Competency Dictionary"
                        extra={[
                            <Button key="3" type="link" onClick={handleAdd}><img src={IconAdd} alt="iconadd"/></Button>,
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload" /></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint" /></Button>
                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
               
                <Dropdown overlay={menuKamus} placement="bottomRight" arrow >
                    <Button>Kamus Kompetensi 2021<DownOutlined style={{ color: 'black', marginLeft:30 }} /></Button>
                </Dropdown>
                <Search placeholder="Search for Competency" onSearch={onSearch} bordered={false} className="searchLearning" style={{ padding: 10, marginTop: 39, borderStyle: 'none' }} />
                <Table dataSource={data}>
                    <Column title="Name" dataIndex="name" key="name" />
                    
                    <Column title="Type" dataIndex="type" key="type" />
                    <Column
                        title="Applied to jobs"
                        dataIndex="appliedtojobs"
                        key="appliedtojobs"
                        render={appliedtojobs => (
                            <>
                                {<a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF' }}>{appliedtojobs}</Text></a>}
                            </>
                        )}
                    />
                </Table>
                {/* <Pagination defaultCurrent={6} total={500} /> */}
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
                </Dropdown>
            </Card>
        </div >
    )
}

export default MyTrainingComp
