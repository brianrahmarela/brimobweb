import { Card, Table, Button, Divider, Input, Tag, Menu, Dropdown, Typography, PageHeader} from 'antd';
import { useState } from 'react';
import IconBackArrow from '../../assets/icons/icon_back_arrow.svg';
import IconAdd from '../../assets/icons/icon_add.svg';
import IconDownload from '../../assets/icons/icon_download.svg';
import IconPrint from '../../assets/icons/icon_print.svg';
import { DownOutlined } from '@ant-design/icons';

import { Link, useRouteMatch } from "react-router-dom";

const { Column, } = Table;
const menuKamus = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                Kamus Kompetensi 2021
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                Kamus Kompetensi 2020
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                Kamus Kompetensi 2019
            </a>
        </Menu.Item>
    </Menu>
);

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

//button icon handle
const onSearch = (value: any) => console.log(value);

const handleAdd = (value: any) => {
    console.log("add my training");
}
const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}


function JobCompetencyComp() {
    let { url } = useRouteMatch();

    const { Search } = Input;
    const { Text } = Typography;

    const [data] = useState([
        {
            key: '1',
            name: 'Direktur Jenderal Mineral dan Batubara',
            status: ['on-review'],
            competencycount: '12 Competency',
        },
        {
            key: '2',
            name: 'Direktur Pembinaan Pengusahaan Batubara',
            status: ['on-review'],
            competencycount: '16 Competency',
        },
        {
            key: '3',
            name: 'Direktur Pembinaan Pengusahaan Mineral',
            status: ['on-review'],
            competencycount: '15 Competency',
        },
        {
            key: '4',
            name: 'Direktur Pembinaan Program Mineral dan Batubara',
            status: ['on-review'],

            competencycount: '18 Competency',
        },
        {
            key: '5',
            name: 'Direktur Penerimaan Mineral dan Batubara',
            status: ['on-review'],
            competencycount: '28 Competency',
        },
        {
            key: '6',
            name: 'Direktur Teknik dan Lingkungan Mineral dan Batubara',
            status: ['on-review'],
            competencycount: '25 Competency',
        },
        {
            key: '7',
            name: 'Sekretaris Direktorat Jenderal Mineral dan batubara',
            status: ['on-finalisasi'],
            competencycount: '18 Competency',
        }, {
            key: '8',
            name: 'tujuh',
            status: ['on-finalisasi'],
            competencycount: '18 Competency',
        }, {
            key: '9',
            name: 'delapan',
            status: ['on-review'],
            competencycount: '18 Competency',
        }, {
            key: '10',
            name: 'sembilan',
            status: ['on-review'],
            competencycount: '18 Competency',
        },
        {
            key: '11',
            name: 'sepuluh',
            status: ['on-review'],
            competencycount: '18 Competency85 position',
        },
        {
            key: '12',
            name: 'sebelas',
            status: ['on-review'],
            competencycount: '85 position',
        },
    ]);

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: '3px', padding: '20px' }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Job Competency"
                        extra={[
                            <Button key="3" type="link" onClick={handleAdd}><img src={IconAdd} alt="iconadd"/></Button>,
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload" /></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint" /></Button>
                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>

                <Dropdown overlay={menuKamus} placement="bottomRight" arrow >
                    <Button>Kompetensi Jabatan 2021<DownOutlined style={{ color: 'black', marginLeft: 30 }} /></Button>
                </Dropdown>
                <Search placeholder="Search for Job" onSearch={onSearch} bordered={false} className="searchLearning" style={{ padding: 10, marginTop: 39, borderStyle: 'none' }} />
                <Table dataSource={data}
                onRow={(record, rowIndex) => {
                    return {
                      onClick: () => {
                          console.log("row clicked");
                      }, // click row
           
                    };
                  }}
                >
                    
                    <Column
                        title="Name"
                        dataIndex="name"
                        key="name"
                        render={(name) => (
                            <>
                                {
                                    name ? (
                                        <Link to={{
                                            pathname: `${url}/detailnama/`,
                                            state: {valListJobCompetency: {name}}
                                       }} style={{ color: "black", fontSize: 13 }}>{name}</Link>                                      
                                    ) : (
                                        <p>No data found</p>
                                    )
                                }
                            </>
                        )}
                    />
                    <Column title="Status" dataIndex="status" key="status"
                        render={status => (
                            <>
                                {status.map((tag: any) => (
                                    tag === "on-review" ?
                                        <Tag color="orange" key={tag}>
                                            {tag}
                                        </Tag> :
                                        tag === "on-finalisasi" ?
                                            <Tag color="cyan" key={tag}>
                                                {tag}
                                            </Tag> :
                                            <Tag color="red" key={tag}>
                                                {tag}
                                            </Tag>
                                ))}
                            </>
                        )}

                    />
                    <Column
                        title="Competency Count"
                        dataIndex="competencycount"
                        key="competencycount"
                        render={competencycount => (
                            <>
                                {<a href="/home" style={{ color: "#3366FF" }}><Text underline style={{ color: '#3366FF' }}>{competencycount}</Text></a>}
                            </>
                        )}
                    />
                </Table>
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button>Sort By Date <DownOutlined style={{ color: 'black' }} /></Button>
                </Dropdown>
            </Card>
        </div >
    )
}

export default JobCompetencyComp
