import { Card, Row, Col, PageHeader, Button, Divider, Table, Typography, Modal, List, Empty } from "antd";
import { useLocation } from "react-router-dom"
import React, { useState } from 'react';

import IconBackArrow from "../../assets/icons/icon_back_arrow.svg";
import IconAdd from '../../assets/icons/icon_add.svg'
import IconDownload from '../../assets/icons/icon_download.svg';
import IconPrint from '../../assets/icons/icon_print.svg';

import TableKompetensiTeknis from "./TableKompetensiTeknis";

const { Column, } = Table;
const data = [
    {
        key: '1',
        name: 'Integritas',
        proficiencylevel: '5',
    },
    {
        key: '2',
        name: 'Kerjasama',
        proficiencylevel: '5',
    },
    {
        key: '3',
        name: 'Komunikasi',
        proficiencylevel: '5',
    },
    {
        key: '4',
        name: 'Orientasi Pada Hasil',
        proficiencylevel: '5',
    },
    {
        key: '5',
        name: 'Pelayanan Publik',
        proficiencylevel: '5',
    },
    {
        key: '6',
        name: 'Mengelola Perubahan',
        proficiencylevel: '5',
    },

];


const dataList = [
    {
        title: 'Tingkat 1',
        description: 'Kemampuan menyebutkan kembali informasi atau pengetahuan yang tersimpan dalam ingatan. Contoh: menyebutkan arti taksonomi. Mendistribusikan, Mengidentifikasi, Mengumpulkan, Menyusun, Menyusun daftar'
    },
    {
        title: 'Tingkat 2',
        description: 'Kemampuan memahami instruksi dan menegaskan pengertian/makna ide atau konsep yang telah diajarkan baik dalam bentuk lisan, tertulis, maupun grafik/diagram Contoh : Merangkum materi yang telah diajarkan dengan kata-kata sendiri Melaporkan, Memahami, Mendiskusikan, Menelaah, Mengkonsepkan, Menyeleksi'
    },
    {
        title: 'Tingkat 3',
        description: 'Kemampuan melakukan sesuatu dan mengaplikasikan konsep dalam situasi tetentu. Contoh: Melakukan proses pembayaran gaji sesuai dengan sistem berlaku. Berperan aktif, Melakukan,Memeriksa, Menerapkan, Mengelola,Menggambar, Mengkoordinasikan, Mengolah,Mengoperasikan, Menugaskan, Menyiapkan, Merumuskan'
    },
    {
        title: 'Tingkat 4',
        description: 'Kemampuan memisahkan konsep kedalam beberapa komponen dan mnghubungkan satu sama lain untuk memperoleh pemahaman atas konsep tersebut secara utuh. Contoh: Menganalisis penyebab meningkatnya Harga pokok penjualan dalam laporan keuangan dengan memisahkan komponen- komponennya. Mengkaji ulang, Mengorganisir'
    },
    {
        title: 'Tingkat 5',
        description: 'Kemampuan menetapkan derajat sesuatu berdasarkan norma, kriteria atau patokan tertentu Contoh: Membandingkan hasil ujian siswa dengan kunci jawaban. Memonitoring, Mendelegasikan, Mendukung, Mengevaluasi, Mengkoreksi, Mereview'
    },
];



//button icon handle
const handleAdd = (value: any) => {
    console.log("add my training");
}
const handleDownload = (value: any) => {
    console.log("Download My Training");
}
const handlePrint = (value: any) => {
    console.log("Print My Training");
}


function JobCompetencyDetailComp() {
    const { Text } = Typography;
    //ambil props list data dari jobCompetency
    const location: any = useLocation()
    const valListJobCompetency = location.state?.valListJobCompetency
    // console.log(valListJobCompetency)

    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = (proficiencylevel: any) => {
        // console.log(e);
        // console.log(proficiencylevel);
        setIsModalVisible(true);
        return proficiencylevel;
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
            <Card bordered={false} style={{ borderRadius: "3px", padding: "20px" }}>
                <div className="site-page-header-ghost-wrapper">
                    <PageHeader
                        ghost={false}
                        backIcon={<img src={IconBackArrow} alt="iconbackarrow" />}
                        onBack={() => window.history.back()}
                        title="Job Competency"
                        extra={[
                            <Button key="3" type="link" onClick={handleAdd}><img src={IconAdd} alt="iconadd" /></Button>,
                            <Button key="1" type="link" onClick={handleDownload}><img src={IconDownload} alt="icondownload" /></Button>,
                            <Button key="2" type="link" onClick={handlePrint}><img src={IconPrint} alt="iconprint" /></Button>
                        ]}
                    >
                    </PageHeader>
                    <Divider style={{ border: '3px solid #5067AA', backgroundColor: '#5067AA', }} />
                </div>
                <Row >
                    <Col span={20}>
                        <Row justify="space-between" style={{ color: "#3F5AA6", fontSize: 18 }}>
                            <Col xl={4} md={9}>
                                <Row>
                                    <p style={{ paddingRight: 90 }}>
                                        Posisi
                                    </p>
                                    <p >
                                        :</p>
                                </Row>
                            </Col>
                            <Col xl={20} md={15}>
                                {valListJobCompetency.name}
                            </Col>
                        </Row>
                    </Col>

                    <Col span={4} style={{ marginBottom: 57, color: "#0022FF", fontSize: 15 }}>
                        <Row justify="end">
                            on-review
                        </Row>
                    </Col>
                </Row>

                <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
                    <h4 style={{ marginBottom: 20 }}>Kompetensi Managerial</h4>
                    <Table dataSource={data} bordered={true} id="trheader">
                        {/* <Column title="Name" dataIndex="name" key="name" /> */}
                        <Column
                            title="Name"
                            dataIndex="name"
                            key="name"
                            render={(name) => (
                                <>{name}</>
                            )}
                        />

                        <Column
                            title="Proficiency Level"
                            dataIndex="proficiencylevel"
                            key="proficiencylevel"
                            render={(proficiencylevel) => (
                                <>
                                    {proficiencylevel ? (
                                        <div>
                                            <Button type="link" style={{ margin: 0, padding: 0 }}> <Text underline style={{ color: '#3366FF' }} onClick={() => showModal(proficiencylevel)}>{proficiencylevel}</Text></Button>
                                            <Modal
                                                // style={{marginRight: 20}}
                                                title={<div style={{ fontSize: 17, fontWeight: 600, }}>Tingkat Kemahiran</div>}
                                                centered
                                                visible={isModalVisible}
                                                onOk={handleOk}
                                                onCancel={handleCancel}
                                                width={1131}
                                                footer={null}
                                                // mask={false}
                                                maskStyle={{ opacity: 0.1 }}
                                            >
                                                <List
                                                    itemLayout="horizontal"
                                                    dataSource={dataList}
                                                    renderItem={item => (

                                                        <>
                                                            {
                                                                item.title <= "Tingkat 4" ? (
                                                                    <List.Item>
                                                                        <List.Item.Meta

                                                                            title={<p style={{ backgroundColor: "#707070", color: "white", borderRadius: 8, paddingLeft: 10, width: 90, }}>{item.title}</p>}
                                                                            description={<p style={{ color: " #707070" }}>{item.description}</p>}
                                                                        />

                                                                    </List.Item>
                                                                ) : (
                                                                    <List.Item>
                                                                        <List.Item.Meta

                                                                            title={<p style={{ backgroundColor: "#4C62A2", color: "white", borderRadius: 8, paddingLeft: 10, width: 90, }}>{item.title}</p>}
                                                                            description={<p style={{ color: " #4C62A2" }}>{item.description}</p>}
                                                                        />

                                                                    </List.Item>
                                                                )
                                                            }
                                                        </>

                                                    )}
                                                />
                                            </Modal>
                                        </div>
                                    ) : (
                                        <Empty />

                                    )}
                                </>
                            )}

                        />
                    </Table>
                </div>

                <TableKompetensiTeknis />


            </Card>
        </div>
    );
}

export default JobCompetencyDetailComp;
