import {
  Button, Table, Typography, Modal, List, Empty 
} from "antd";
import React, { useState } from 'react';

const { Column, } = Table;
const data2 = [
  {
      key: '1',
      name: 'Manajemen Keuangan',
      proficiencylevel: '4',
  },
  {
      key: '2',
      name: 'Manajemen Proyek',
      proficiencylevel: '4',
  },
  {
      key: '3',
      name: 'Evaluasi Permasalahan Tambang',
      proficiencylevel: '5',
  },
  {
      key: '4',
      name: 'Membuat Kebijakan di bidang Tambang',
      proficiencylevel: '5',
  },
  {
      key: '5',
      name: 'Kerjasama antar Departemen',
      proficiencylevel: '5',
  },
  {
      key: '6',
      name: 'Evaluasi Project ',
      proficiencylevel: '5',
  },

];
const dataList2 = [
  {
      title: 'Tingkat 1',
      description: 'Kemampuan menyebutkan kembali informasi atau pengetahuan yang tersimpan dalam ingatan. Contoh: menyebutkan arti taksonomi. Mendistribusikan, Mengidentifikasi, Mengumpulkan, Menyusun, Menyusun daftar'
  },
  {
      title: 'Tingkat 2',
      description: 'Kemampuan memahami instruksi dan menegaskan pengertian/makna ide atau konsep yang telah diajarkan baik dalam bentuk lisan, tertulis, maupun grafik/diagram Contoh : Merangkum materi yang telah diajarkan dengan kata-kata sendiri Melaporkan, Memahami, Mendiskusikan, Menelaah, Mengkonsepkan, Menyeleksi'
  },
  {
      title: 'Tingkat 3',
      description: 'Kemampuan melakukan sesuatu dan mengaplikasikan konsep dalam situasi tetentu. Contoh: Melakukan proses pembayaran gaji sesuai dengan sistem berlaku. Berperan aktif, Melakukan,Memeriksa, Menerapkan, Mengelola,Menggambar, Mengkoordinasikan, Mengolah,Mengoperasikan, Menugaskan, Menyiapkan, Merumuskan'
  },
  {
      title: 'Tingkat 4',
      description: 'Kemampuan memisahkan konsep kedalam beberapa komponen dan mnghubungkan satu sama lain untuk memperoleh pemahaman atas konsep tersebut secara utuh. Contoh: Menganalisis penyebab meningkatnya Harga pokok penjualan dalam laporan keuangan dengan memisahkan komponen- komponennya. Mengkaji ulang, Mengorganisir'
  },
  {
      title: 'Tingkat 5',
      description: 'Kemampuan menetapkan derajat sesuatu berdasarkan norma, kriteria atau patokan tertentu Contoh: Membandingkan hasil ujian siswa dengan kunci jawaban. Memonitoring, Mendelegasikan, Mendukung, Mengevaluasi, Mengkoreksi, Mereview'
  },
];
function TableKompetensiTeknis() {
  const { Text } = Typography;

  const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = (proficiencylevel: any) => {
        // console.log(e);
        console.log(proficiencylevel);
        setIsModalVisible(true);
        return proficiencylevel;
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
  return (
    <div>
      <div className="site-card-wrapper" style={{ marginBottom: 40 }}>
                    <h4 style={{ marginBottom: 20 }}>Kompetensi Teknis</h4>
                    <Table dataSource={data2} bordered={true} id="trheader2">
                        {/* <Column title="Name" dataIndex="name" key="name" /> */}
                        <Column
                            title="Name"
                            dataIndex="name"
                            key="name"
                            render={(name) => (
                                <>{name}</>
                            )}
                        />

                        <Column
                            title="Proficiency Level"
                            dataIndex="proficiencylevel"
                            key="proficiencylevel"
                            render={(proficiencylevel) => (
                                <>
                                    {proficiencylevel ? (
                                        <div>
                                            <Button type="link" style={{ margin: 0, padding: 0 }}> <Text underline style={{ color: '#3366FF' }} onClick={() => showModal(proficiencylevel)}>{proficiencylevel}</Text></Button>
                                            <Modal
                                                // style={{marginRight: 20}}
                                                title={<div style={{ fontSize: 17, fontWeight: 600, }}>Tingkat Kemahiran</div>}
                                                centered
                                                visible={isModalVisible}
                                                onOk={handleOk}
                                                onCancel={handleCancel}
                                                width={1131}
                                                footer={null}
                                                // mask={false}
                                                maskStyle={{ opacity: 0.1 }}
                                            >
                                                <List
                                                    itemLayout="horizontal"
                                                    dataSource={dataList2}
                                                    renderItem={item2 => (

                                                        <>
                                                            {
                                                                item2.title === "Tingkat 4" ? (
                                                                    <List.Item>
                                                                        <List.Item.Meta

                                                                            title={<p style={{ backgroundColor: "#4C62A2", color: "white", borderRadius: 8, paddingLeft: 10, width: 90, }}>{item2.title}</p>}
                                                                            description={<p style={{ color: " #4C62A2" }}>{item2.description}</p>}
                                                                        />

                                                                    </List.Item>
                                                                ) : (
                                                                        <List.Item>
                                                                            <List.Item.Meta
                                                                                title={<p style={{ backgroundColor: "#707070", color: "white", borderRadius: 8, paddingLeft: 10, width: 90, }}>{item2.title}</p>}
                                                                                description={<p style={{ color: " #707070" }}>{item2.description}</p>}
                                                                            />

                                                                        </List.Item>
                                                                    )
                                                                }
                                                        </>

                                                    )}
                                                />
                                            </Modal>
                                        </div>
                                    ) : (
                                        <Empty />

                                    )}
                                </>
                            )}

                        />
                    </Table>
                </div>
    </div>
  )
}

export default TableKompetensiTeknis
