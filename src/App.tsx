import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.less';

//pages
import Dashboard from '../src/pages/Dashboard'
//training pages
const TrainingApproval = React.lazy(() => import('./pages/TrainingApproval'));
const MyLearningPath = React.lazy(() => import('./pages/MyLearningPath'));
const MyTraining = React.lazy(() => import('./pages/MyTrainingPage'));
const TrainingRequestPage = React.lazy(() => import('./pages/TrainingRequestPage'));
const TrainingAssignmentPage = React.lazy(() => import('./pages/TrainingAssignmentPage'));
const MyTrainingProfile = React.lazy(() => import('./pages/Profile'));
const Schedule = React.lazy(() => import('./pages/Schedule'));
const CompetencyLearningPath = React.lazy(() => import('./pages/CompetencyLearningPath'));
const TrainingReport = React.lazy(() => import('./pages//TrainingReport'));
const TrainingReview = React.lazy(() => import('./pages//TrainingReview'));
//performance setup pages
const CompetencyDictionary = React.lazy(() => import('./pages/CompetencyDictionary'));
const JobCompetency = React.lazy(() => import('./pages/JobCompetency'));
const JobCompetencyDetail = React.lazy(() => import('./pages/JobCompetencyDetail'))
//Appraisal pages
const KpiPlanSubmision = React.lazy(() => import('./pages/KpiPlanSubmision'));
const KpiPlanApproval = React.lazy(() => import('./pages/KpiPlanApproval'));
const KpiMidYearReview = React.lazy(() => import('./pages/KpiMidYearReview'));
const KpiEndYearReview = React.lazy(() => import('./pages/KpiEndYearReview'));
const KpiSignOff = React.lazy(() => import('./pages/KpiSignOff'));

const TeamAppraisal = React.lazy(() => import('./pages/TeamAppraisal'));
const TeamPerformanceReport = React.lazy(() => import('./pages/TeamPerformanceReport'));
const SubOrdinateAppraisal = React.lazy(() => import('./pages/SubOrdinateAppraisal'));
const SubordinatePerformanceReport = React.lazy(() => import('./pages/SubOrdinatePerformanceReport'));

function App() {
  return (
    <React.Suspense fallback={<p>Loading...</p>}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Dashboard />
          </Route>
          <Route exact path="/mytrainingprofile">
            <MyTrainingProfile />
          </Route>
          <Route exact path="/mylearningpath">
            <MyLearningPath />
          </Route>
          <Route exact path="/mylearningpath/competencyLearningPath">
            <CompetencyLearningPath />
          </Route>
          <Route exact path="/mytraining">
            <MyTraining />
          </Route>
          <Route exact path="/trainingrequest">
            <TrainingRequestPage />
          </Route>
          <Route exact path="/trainingassignment">
            <TrainingAssignmentPage />
          </Route>
          <Route exact path="/trainingschedule">
            <Schedule />
          </Route>
          <Route exact path="/trainingapproval">
            <TrainingApproval />
          </Route>
          <Route exact path="/trainingreport">
            <TrainingReport />
          </Route>
          <Route exact path="/trainingreview">
            <TrainingReview />
          </Route>
          <Route exact path="/competencydictionary">
            <CompetencyDictionary />
          </Route>
          <Route exact path="/jobcompetency">
            <JobCompetency />
          </Route>
          <Route path="/jobcompetency/detailnama/">
            <JobCompetencyDetail />
          </Route>
          <Route path="/kpiplansubmission">
            <KpiPlanSubmision />
          </Route>
          <Route path="/kpiplanapproval">
            <KpiPlanApproval />
          </Route>
          <Route path="/kpimidyearreview">
            <KpiMidYearReview />
          </Route>
          <Route path="/kpiendyearreview">
            <KpiEndYearReview />
          </Route>
          <Route path="/kpisignoff">
            <KpiSignOff />
          </Route>
          <Route path="/teamappraisal">
            <TeamAppraisal />
          </Route>
          <Route path="/teamperformancereport">
            <TeamPerformanceReport />
          </Route>
          <Route path="/subordinateappraisal">
            <SubOrdinateAppraisal />
          </Route>
          <Route path="/subordinateperformancereport">
            <SubordinatePerformanceReport />
          </Route>
        </Switch>
      </Router>
    </React.Suspense>
  );
}

export default App;

