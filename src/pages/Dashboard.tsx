import React from 'react';
import { Layout} from 'antd';

//Components
import Navbar from '../components/Navbar';
class Dashboard extends React.Component {
    state = {
        collapsed: false,
        size: 8
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Layout className="site-layout " >
                <Navbar />
            </Layout>
        );
    }
}
export default Dashboard;