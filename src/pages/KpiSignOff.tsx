import React from 'react';
import { Layout, Menu, Divider, Typography, Row, Col, Dropdown, Badge, Avatar, Image, } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import DashboardIcon from '../assets/icons/icon_dashboard.svg';
import HomeIcon from '../assets/icons/icon_home.svg';
import LogoBrimob from '../assets/images/logo_brimob.svg';
import IconNotif from '../assets/icons/icon_notif.svg';
import IconHomeGrey from '../assets/icons/icon_home_grey.svg';

import IconLearning from '../assets/icons/icon_learning.svg';
import IconPerformance from '../assets/icons/icon_performance.svg';
import ImgProfilePict from '../assets/images/profile_pict.png';
import IconMenuAppraisal from '../assets/icons/icon_menu_appraisal.svg';
import { DownOutlined } from '@ant-design/icons';
import { Link, } from "react-router-dom";

//Components
import FooterDashboard from '../components/dashboard_comp/FooterDashboard';
// import KpiEndYearReviewComp from '../components/Appraisal/MyAppraisal/kpiEndYearReviewComp';
import KpiSignOffComp from '../components/Appraisal/MyAppraisal/kpiSignOffComp';
const { Header, Sider, Content, Footer } = Layout;
const { Title, Text } = Typography;

const menu = (

    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                ENGLISH
            </a>
        </Menu.Item>
    </Menu>
);
const { SubMenu } = Menu;

class KpiSignOff extends React.Component {
    //  history = useHistory();
    state = {
        collapsed: false,
        size: 8
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Layout className="site-layout " >
                <Header className="site-layout-background container" >
                    <Row justify="space-between" >
                        <Col xs={6} sm={12} md={12} lg={14} span={14}>

                            <Row align="middle" gutter={[16, 16]} justify="start">
                                <Col key="1" xs={6} sm={2} md={2} lg={2} xl={1} xxl={1}> {React.createElement(this.state.collapsed ? MenuOutlined : MenuOutlined, {
                                    className: 'trigger',
                                    onClick: this.toggle,
                                })}
                                </Col>

                                <Col key="2" style={{ paddingRight: 20 }} xs={18} sm={7} md={4} lg={3} xl={2} xxl={1}>
                                    <Image src={LogoBrimob} preview={false} alt="logobrimobs" />
                                </Col>
                                <Col key="3" xs={0} sm={0} md={17} lg={19} xl={21} xxl={21}>
                                    <Title >S.O.K BRIMOB</Title>
                                </Col>

                            </Row>
                        </Col>
                        <Col xs={18} sm={12} md={12} lg={10} span={10}>

                            <Row align="middle" gutter={[16, 16]} justify="end">
                                <Col key="4" >
                                    <Dropdown overlay={menu}>
                                        <span style={{ color: "white", fontWeight: 300 }} className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                            INDONESIA<DownOutlined />
                                        </span>
                                    </Dropdown>
                                </Col>
                                <Col key="5" >
                                    <Badge count={2} >
                                        <Avatar shape="circle" size="large" style={{ paddingTop: 10 }} icon={<Image src={IconNotif} preview={false} height={20} />} />
                                    </Badge>
                                </Col>
                                <Col key="6" >
                                    <Badge >
                                        <Avatar shape="circle" size="large" icon={<Image src={ImgProfilePict} preview={false} />} />
                                    </Badge>
                                </Col>
                                <Col key="7">
                                    <Text style={{ fontWeight: 300 }}>Hendrik</Text>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Header>

                <Layout>
                    <Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{ marginRight: 6 }} width={250}>
                        <div className="logo" />
                        <Menu theme="light" mode="inline" defaultSelectedKeys={['1']} style={{ marginTop: 39 }}>
                            <Menu.Item key="1" icon={<img alt="dashboardicon" src={DashboardIcon} />}><Link to="/">Dashboard</Link>

                            </Menu.Item>
                            <div>
                                <Divider style={{ margin: 0, padding: 0 }} />
                            </div>
                            <Menu.Item key="2" icon={<img alt="homeicons" src={HomeIcon} />}>
                                Home
                            </Menu.Item>
                            <div>
                                <Divider style={{ margin: 0, padding: 0 }} />
                            </div>
                            <SubMenu key="sub1" icon={<img alt="homeicons" src={IconLearning} />} title="Learning & Training">
                                <Menu.Item key="3"><Link to="/mytrainingprofile">Profile</Link></Menu.Item>
                                <Menu.Item key="4"><Link to="/mylearningpath">My Learning Path</Link></Menu.Item>
                                <Menu.Item key="5"><Link to="/mytraining">My Training</Link></Menu.Item>
                                <Menu.Item key="6"><Link to="/trainingrequest">Training Request</Link></Menu.Item>
                                <Menu.Item key="7"><Link to="/trainingassignment">Training Assignment</Link></Menu.Item>
                                <Menu.Item key="8"><Link to="/trainingschedule">Schedule</Link></Menu.Item>
                                <Menu.Item key="9"><Link to="/trainingapproval">Training Approval</Link></Menu.Item>
                                <Menu.Item key="10"><Link to="/trainingreport">Training Report</Link></Menu.Item>
                                <Menu.Item key="11"><Link to="/trainingreview">Training Review</Link></Menu.Item>
                            </SubMenu>

                            <div>
                                <Divider style={{ margin: 0, padding: 0 }} />
                            </div>
                            <SubMenu key="sub2" icon={<img alt="homeicons" src={IconPerformance} style={{
                                height: 20
                            }} />} title="Performance Setup">
                                <Menu.Item key="12"><Link to="/competencydictionary">Competency Dictionary</Link></Menu.Item>
                                <Menu.Item key="13"><Link to="/jobcompetency">Job Competency</Link></Menu.Item>
                            </SubMenu>
                            <div>
                                <Divider style={{ margin: 0, padding: 0 }} />
                            </div>
                            <SubMenu key="sub3" icon={<img alt="homeicons" src={IconMenuAppraisal} style={{
                                height: 18
                            }} />} title="Appraisal">
                                <SubMenu key="sub" title="My Appraisal">
                                    <Menu.Item key="14"><Link to="/kpiplansubmission">KPI Plan Submision</Link></Menu.Item>
                                    <Menu.Item key="15"><Link to="/kpiplanapproval">KPI Plan Approval</Link></Menu.Item>
                                    <Menu.Item key="16"><Link to="/kpimidyearreview">KPI Mid Year Review</Link></Menu.Item>
                                    <Menu.Item key="17"><Link to="/kpiendyearreview">KPI End Year Review</Link></Menu.Item>
                                    <Menu.Item key="18"><Link to="/Kpisignoff">KPI Sign Off</Link></Menu.Item>
                                </SubMenu>
                                <Menu.Item key="19"><Link to="/teamappraisal">Team Appraisal</Link></Menu.Item>
                                <Menu.Item key="20"><Link to="/teamperformancereport">Team Performance Report</Link></Menu.Item>
                                <Menu.Item key="21"><Link to="/subordinateappraisal">Subordinate Appraisal</Link></Menu.Item>
                                <Menu.Item key="22"><Link to="/subordinateperformancereport">Subordinate Performance Report</Link></Menu.Item>
                            </SubMenu>
                            <div>
                                <Divider style={{ margin: 0, padding: 0 }} />
                            </div>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Row align="middle" style={{ paddingTop: 20.6, marginLeft: 20 }}>
                            <Col flex="28px">
                            </Col>
                            <Col flex="27px">
                                <img alt="homeicons" src={IconHomeGrey} height={18.39} />
                            </Col>
                            <Col flex="auto" style={{ paddingTop: 5 }}>
                                <Text style={{ color: "#707070", fontWeight: 500 }}>
                                    Appraisal / KPI Plan / KPI Sign Off
                                </Text>
                            </Col>
                        </Row>
                        <Content
                            className="site-layout-background"
                            style={{
                                margin: '24px 16px 0px 46px',
                                padding: 0,
                                background: 'none'
                            }}
                        >
                            <div className="site-card-wrapper">
                                <Row gutter={[16, 16]}>

                                </Row>
                            </div>
                            <KpiSignOffComp />
                        </Content>
                        <Footer><FooterDashboard /></Footer>
                    </Layout>
                </Layout>
            </Layout>
        );
    }
}
export default KpiSignOff;